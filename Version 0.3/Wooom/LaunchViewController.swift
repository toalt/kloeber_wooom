//
//  LaunchViewController.swift
//  Wooom
//
//  Created by Tobias Alt on 10.08.18.
//  Copyright © 2018 Kloeber. All rights reserved.
//

import UIKit
import WebKit

class LaunchViewController: UIViewController,WKUIDelegate {
    
    var launchWebView: WKWebView!
    
    @IBOutlet weak var popUpView: UIView!
    @IBOutlet weak var reconBtn: UIButton!
    // Notication Center for update device state
    let nc = NotificationCenter.default
    
    var blurEffectView:UIView!
    
    // Create a global instance of NSUserDefaults class
    let preferences = UserDefaults.standard
    
    // Corparate color
    let primaryColor = UIColor(rgb: 0xFFBB02)
    
    // Count touches on recon Btn
    var countTouch = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let webConfiguration = WKWebViewConfiguration()
        
        launchWebView = WKWebView(frame: view.bounds, configuration: webConfiguration)
        launchWebView.uiDelegate = self
        
        view.addSubview(launchWebView)
        
        // Load WKWebView resources
        let htmlPath = Bundle.main.path(forResource: "launchDE", ofType: "html")
        let htmlUrl = URL(fileURLWithPath: htmlPath!, isDirectory: false)
        launchWebView.loadFileURL(htmlUrl, allowingReadAccessTo: htmlUrl)
        
        // Make background transparant and fit full screen
        launchWebView.isOpaque = false
        launchWebView.backgroundColor = UIColor.clear
        launchWebView.scrollView.bounces = false
        launchWebView.scrollView.contentInsetAdjustmentBehavior = .never
        
        // Add Observer for listing to battery voltage characteristc
        nc.addObserver(self, selector: #selector(connectionEstablished(_:)), name: Notification.Name("BLEConnected"), object: nil)
        //nc.addObserver(self, selector: #selector(timeOut(_:)), name: Notification.Name("Timeout"), object: nil)
        
        //------ Variante checken fehlt im Launch VC --------
        
        // Check wheter paired devices is stored or scan new one from tag
        if let storedDeviceName = preferences.object(forKey: "pairedDevice") {
            print (storedDeviceName)
            // if stored peripheral name exists - then connect
            BLEDevice.shared.initCBManager()
        } else {
            //show screen 2 sec before enter app
            DispatchQueue.main.asyncAfter(deadline: .now()+3) { //desired number of seconds
                self.performSegue(withIdentifier: "launchToNotification", sender: nil)
            }
        }
        
        // Style for blur effect when disconnected
        reconBtn.backgroundColor = primaryColor
        reconBtn.layer.cornerRadius = 10
        
        // Launch after 3 sec logo screen
        DispatchQueue.main.asyncAfter(deadline: .now()+3) { //desired number of seconds
            self.performSegue(withIdentifier: "launchToNotification", sender: nil)
        }
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    // Notification function called when device is connected
    @objc func connectionEstablished(_ notificaiton:Notification) {
        
        if let storedDeviceType: Int = preferences.object(forKey: "version") as? Int{

            if (storedDeviceType == 2) {
                self.performSegue(withIdentifier: "launchVersion", sender: nil)
            } else {
                self.performSegue(withIdentifier: "launchToApp", sender: nil)
            }
            
        } else {
            self.performSegue(withIdentifier: "launchToApp", sender: nil)
        }
    }
    
    // Notification function called when scan interval timed out
    @objc func timeOut(_ notificaiton:Notification) {
        // Only show popUp when VC is visible -> VC1 stay alive for unwind
        if self.isViewLoaded && (self.view.window != nil) {
            if (countTouch < 1) {
                timeOutInterval()
            } else {
                // delete shared preferences
                preferences.removeObject(forKey: "pairedDevice")
                preferences.removeObject(forKey: "version")
                //print("Lösche Device Namen")
                self.performSegue(withIdentifier: "launchToNotification", sender: nil)
            }
        }
    }
    
    func timeOutInterval() {
        //blur background
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.regular)
        blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.view.addSubview(blurEffectView)
        
        popUpView.center = self.view.center
        popUpView.layer.cornerRadius = 10
        self.view.addSubview(popUpView)
    }
    
    @IBAction func onReconBtn(_ sender: Any) {
        if (countTouch < 1) {
            hidePopUpView()
            BLEDevice.shared.initCBManager()
        }
        // increment variable for timeout loop
        countTouch+=1
    }
    
    @IBAction func unwindToVC1(segue:UIStoryboardSegue) {

        // Set countTouch to 1 -> deny showing recon Butn 2 times
        countTouch = 1

        // Check wheter paired devices is stored or scan new one from tag
        if let storedDeviceName = preferences.object(forKey: "pairedDevice") {
            print (storedDeviceName)
            // if stored peripheral name exists - then connect
            BLEDevice.shared.initCBManager()
        } else {
            // Show screen 3 sec before enter app
            DispatchQueue.main.asyncAfter(deadline: .now()+3) { //desired number of seconds
                self.performSegue(withIdentifier: "launchToNotification", sender: nil)
            }
        }
        
        // Launch after 3 sec
        DispatchQueue.main.asyncAfter(deadline: .now()+3) { //desired number of seconds
            self.performSegue(withIdentifier: "launchToNotification", sender: nil)
        }
        
    }
    
    func hidePopUpView() {
        blurEffectView.removeFromSuperview()
        popUpView.removeFromSuperview()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
