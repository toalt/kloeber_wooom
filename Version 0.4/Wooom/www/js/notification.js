
function scanNFCTag() {
    window.webkit.messageHandlers.nfcScan.postMessage("NFCScan");
}

function activityIndicator() {
    document.body.style.backgroundImage = "url('background.jpg')";
    document.body.style.backgroundSize = "cover";
    var content = document.getElementById("slider");
    content.style.filter = "blur(20px)";
    
    var navi = document.getElementById("navi");
    navi.style.filter = "blur(20px)";
    
    var spin = document.getElementById("spinningWheel");
    spin.className = "isShown";
}

function connSuccess() {
    /*stop spinning wheel */
    var spin = document.getElementById("spinningWheel");
    spin.className = "isHidden";
    /*clear content*/
    var content = document.getElementById("slider");
    content.className = "hideContent";
    var naviBar = document.getElementById("navi");
    naviBar.className = "hideContent";
    
    var success = document.getElementById("connSuccess");
    success.className = 'show';
    
    /*alert native swift code*/
    window.webkit.messageHandlers.connection.postMessage("Ready");
}
