var chair = document.getElementById("chair");
var modusBar = document.getElementById("modus");
var buttonOne = document.getElementById("btnOne");
var buttonTwo = document.getElementById("btnTwo");
var buttonThree = document.getElementById("btnThree");
var buttonFour = document.getElementById("btnFour");


function enableModus() {
    modusBar.style.opacity = 0.9;
    chair.style.opacity = 0.9;
    buttonOne.style.pointerEvents = "auto";
    buttonTwo.style.pointerEvents = "auto";
    buttonThree.style.pointerEvents = "auto";
    buttonFour.style.pointerEvents = "auto";
}

function disableModus() {
    modusBar.style.opacity = 0.3;
    chair.style.opacity = 0.3;
    buttonOne.style.pointerEvents = "none";
    buttonTwo.style.pointerEvents = "none";
    buttonThree.style.pointerEvents = "none";
    buttonFour.style.pointerEvents = "none";
}

function setModus(modus) {
    
    switch (modus) {
        case "one":
            buttonOne.style.background = "#FFBB02";
            buttonTwo.style.background = "white";
            buttonThree.style.background = "white";
            buttonFour.style.background = "white";
            //send to native controller
            window.webkit.messageHandlers.modusOne.postMessage("Modus1");
            break;
        case "two":
            buttonOne.style.background = "white";
            buttonTwo.style.background = "#FFBB02";
            buttonThree.style.background = "white";
            buttonFour.style.background = "white";
            window.webkit.messageHandlers.modusTwo.postMessage("Modus2");
            break;
        case "three":
            buttonOne.style.background = "white";
            buttonTwo.style.background = "white";
            buttonThree.style.background = "#FFBB02";
            buttonFour.style.background = "white";
            window.webkit.messageHandlers.modusThree.postMessage("Modus3");
            break;
        case "four":
            buttonOne.style.background = "white";
            buttonTwo.style.background = "white";
            buttonThree.style.background = "white";
            buttonFour.style.background = "#FFBB02";
            window.webkit.messageHandlers.modusFour.postMessage("Modus4");
            break;
    }
}
