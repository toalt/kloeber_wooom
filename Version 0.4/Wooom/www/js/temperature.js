var chair = document.getElementById("chair");
var boostBtn = document.getElementById("boost");
//variable to interrupt
var timeout;
var colorInterval;
//counter variable
var countColor = 0;
var countFont = 0;

function enableButton() {
    boostBtn.disabled = false;
    chair.style.opacity = 0.9;
}

function disableButton() {
    boostBtn.disabled = true;
    chair.style.opacity = 0.3;
}

function boostUp() {
    chair.style.background = "url(boost.svg)";
    chair.style.backgroundRepeat = "no-repeat";
    chair.style.backgroundPosition = "center center";
    //send to native controller
    window.webkit.messageHandlers.boost.postMessage("Boost");
    
    //set background color on start boost
    boostBtn.style.background = "#FFBB02";
    boostBtn.style.fontWeight = "bolder";
    colorInterval = setInterval(changeColor, 750);
    
    //disable button while boost process - remove highlighted style
    boostBtn.disabled = true;
    timeout = setTimeout(function(){
        clearInterval(colorInterval);
        chair.style.background = "url(chair.svg)";
        chair.style.backgroundRepeat = "no-repeat";
        chair.style.backgroundPosition = "center center";
        boostBtn.disabled = false;
        boostBtn.style.background = "transparent";
        boostBtn.style.fontWeight = "normal";
        window.webkit.messageHandlers.boostEnd.postMessage("Boost beendet");
    },30000)
}

function interruptBoost() {
    clearTimeout(timeout);
    chair.style.background = "url(chair.svg)";
    chair.style.backgroundRepeat = "no-repeat";
    chair.style.backgroundPosition = "center center";
    boostBtn.disabled = true;
    boostBtn.style.background = "transparent";
    boostBtn.style.fontWeight = "normal";
    clearInterval(colorInterval);
    window.webkit.messageHandlers.boostEnd.postMessage("Boost beendet");
}

function changeColor() {
    var color = ["transparent", "#FFBB02"];
    var font = ["normal", "bolder"];
    boostBtn.style.backgroundColor = color[countColor];
    boostBtn.style.fontWeight = color[countFont];
    countColor = (countColor + 1) % color.length;
    countFont = (countFont + 1) % font.length;
}
