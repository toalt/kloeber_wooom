//
//  FirstViewController.swift
//  Wooom
//
//  Created by Tobias Alt on 07.08.18.
//  Copyright © 2018 Kloeber. All rights reserved.
//

/////////////////////////////////////////////////////////////////////////////////////////
// History: V 0.1: Project start and testing for upload to iTunes Connect              //
//          V 0.2: Prototype version for Kloeber OrgaTec                               //
//                 - BlE functions enabled -> read/write                               //
//                 - No animations, no warnings, no custom user prefrences             //
//          V 0.3: Retrospective changes for Kloeber OrgaTec                           //
//                 - Detect when user left the chair                                   //
//                 - Slider touch handling                                             //
//                 - BLE Conncetion State Handling with PopUp Exceptions and Timer     //
//                 - Seagues now -> Unwind                                             //
//                 - Bluetooth state detection -> use case off -> reinit CBmanager     //
//          V 0.4: - Temperatur values halved (/2) -> boost not halved                 //
//                 - read chars -1 -> 1-60min // 1-250h // 1-101 voltage               //
//                 - paired devices removed -> alway launch notification screen        //
//                 - product variante check -> in notification VC added                //
//                 - App icon replaced with logo                                       //
/////////////////////////////////////////////////////////////////////////////////////////


import UIKit
import WebKit

class LightViewController: UIViewController,WKUIDelegate {

    // WKWebView for chair SVG
    var lightWebView: WKWebView!
    
    // IBOutlets
    @IBOutlet weak var stateSwitch: UISwitch!
    @IBOutlet weak var webContainer: UIView!
    @IBOutlet weak var dimSlider: UISlider!
    @IBOutlet weak var dimLabel: UILabel!
    @IBOutlet weak var tempSlider: UISlider!
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var popUpView: UIView!
    @IBOutlet weak var dialogButton: UIButton!
    @IBOutlet weak var popUpView2: UIView!
    @IBOutlet weak var reconBtn: UIButton!
    
    //corparate color
    let primaryColor = UIColor(rgb: 0xFFBB02)
    
    // Notication Center for update device state
    let nc = NotificationCenter.default
    
    // Create a global instance of NSUserDefaults class
    let preferences = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Color the switch background
        stateSwitch.backgroundColor = UIColor.gray
        stateSwitch.layer.cornerRadius = 16.0
        
        // Add a gesture recognizer to the slider
        let tapGestureRecognizerDim = UITapGestureRecognizer(target: self, action: #selector(dimSliderTapped(gestureRecognizer:)))
        dimSlider.addGestureRecognizer(tapGestureRecognizerDim)
        let tapGestureRecognizerTemp = UITapGestureRecognizer(target: self, action: #selector(tempSliderTapped(gestureRecognizer:)))
        tempSlider.addGestureRecognizer(tapGestureRecognizerTemp)
        
        // Set slider thumb image
        dimSlider.setThumbImage(#imageLiteral(resourceName: "thumb"), for: .normal)
        tempSlider.setThumbImage(#imageLiteral(resourceName: "thumb"), for: .normal)
        
        // WKWebView setup
        let webConfiguration = WKWebViewConfiguration()
        // Init WKWebView
        lightWebView = WKWebView(frame: webContainer.bounds, configuration: webConfiguration)
        lightWebView.uiDelegate = self
        // Add to container
        webContainer.addSubview(lightWebView)
        
        // Constraints to fit container layout
        lightWebView.translatesAutoresizingMaskIntoConstraints = false
        lightWebView.leadingAnchor.constraint(equalTo: webContainer.leadingAnchor, constant: 0).isActive = true
        lightWebView.trailingAnchor.constraint(equalTo: webContainer.trailingAnchor, constant: 0).isActive = true
        lightWebView.topAnchor.constraint(equalTo: webContainer.topAnchor, constant: 0).isActive = true
        lightWebView.bottomAnchor.constraint(equalTo: webContainer.bottomAnchor, constant: 0).isActive = true
        
        // Load WKWebView resources
        let htmlPath = Bundle.main.path(forResource: "light", ofType: "html")
        let htmlUrl = URL(fileURLWithPath: htmlPath!, isDirectory: false)
        lightWebView.loadFileURL(htmlUrl, allowingReadAccessTo: htmlUrl)
        // Let WebView appear transparent and fit full content
        lightWebView.isOpaque = false
        lightWebView.backgroundColor = UIColor.clear
        lightWebView.scrollView.bounces = false
        lightWebView.scrollView.contentInsetAdjustmentBehavior = .never
        
        // Style for blur effect when disconnected
        dialogButton.backgroundColor = primaryColor
        dialogButton.layer.cornerRadius = 10
        
        reconBtn.backgroundColor = primaryColor
        reconBtn.layer.cornerRadius = 10
        
        // Add Observer for listing to battery voltage characteristc
        //nc.addObserver(self, selector: #selector(sensorStateChanged(_:)), name: Notification.Name("SensorEvent"), object: nil)
        nc.addObserver(self, selector: #selector(lostConnection(_:)), name: Notification.Name("BLEDisconnected"), object: nil)
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func stateSwitchChanged(_ sender: Any) {
        if (stateSwitch.isOn) {
            dimSlider.isEnabled = true
            tempSlider.isEnabled = true
            dimLabel.textColor = primaryColor
            tempLabel.textColor = primaryColor
            lightWebView.evaluateJavaScript("enableLight();")
            
            //temp slide should start with 50 -> only if no last settings exists
            tempSlider.value = 50
            BLEDevice.shared.writeLedColorValue(value: 50)
            // Load last values from BLE board
            
        } else {
            dimSlider.isEnabled = false
            tempSlider.isEnabled = false
            dimLabel.textColor = UIColor.white
            tempLabel.textColor = UIColor.white
            lightWebView.evaluateJavaScript("disableLight();")
            // Reset LED values on BLE board
            BLEDevice.shared.disableLightTab()
            // Reset GUI values to 0
            dimSlider.value = 0
            tempSlider.value = 0
        }
    }
    
    @IBAction func dimValueChanged(_ sender: Any) {
        let intValueDim:NSInteger = NSInteger(dimSlider.value)
        print ("Dimmer ist auf \(intValueDim)")
        BLEDevice.shared.writeLedBrightValue(value: intValueDim)
    }
    
    @objc func dimSliderTapped(gestureRecognizer: UIGestureRecognizer) {
        let pointTapped: CGPoint = gestureRecognizer.location(in: self.view)
        let positionOfSlider: CGPoint = dimSlider.frame.origin
        let widthOfSlider: CGFloat = dimSlider.frame.size.width
        let newValue = ((pointTapped.x - positionOfSlider.x) * CGFloat(dimSlider.maximumValue) / widthOfSlider)
        dimSlider.setValue(Float(newValue), animated: true)
        
        // Same arg as in value changed event -> BLE value
        let intValueDim:NSInteger = NSInteger(dimSlider.value)
        print ("Dimmer ist auf \(intValueDim)")
        BLEDevice.shared.writeLedBrightValue(value: intValueDim)
    }
    
    @IBAction func tempValueChanged(_ sender: Any) {
        let intValueTemp:Int = Int(tempSlider.value)
        print ("Temperatur ist auf \(intValueTemp)")
        BLEDevice.shared.writeLedColorValue(value: intValueTemp)
    }

    @objc func tempSliderTapped(gestureRecognizer: UIGestureRecognizer) {
        let pointTapped: CGPoint = gestureRecognizer.location(in: self.view)
        let positionOfSlider: CGPoint = tempSlider.frame.origin
        let widthOfSlider: CGFloat = tempSlider.frame.size.width
        let newValue = ((pointTapped.x - positionOfSlider.x) * CGFloat(tempSlider.maximumValue) / widthOfSlider)
        tempSlider.setValue(Float(newValue), animated: true)
        
        // Same arg as in value changed event -> BLE value
        let intValueTemp:NSInteger = NSInteger(tempSlider.value)
        print ("Temperatur ist auf \(intValueTemp)")
        BLEDevice.shared.writeLedColorValue(value: intValueTemp)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Notification function called when device gets disconnected
    @objc func lostConnection(_ notificaiton:Notification) {
        disconnected()
    }
    
    // Notification function called when sensor state changed -> user not sitting?
    /*@objc func sensorStateChanged(_ notificaiton:Notification) {
        //check sensor state
        if (BLEDevice.shared.sensorState == 0) {
            disconnectedFromSensor()
        }
    }*/
    
    /*func disconnectedFromSensor() {
        //blur background
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.regular)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.view.addSubview(blurEffectView)
        
        popUpView.center = self.view.center
        popUpView.layer.cornerRadius = 10
        self.view.addSubview(popUpView)
        
        // force the user to push the btn
        tabBarController?.tabBar.isHidden = true
        // disconnect the device
        BLEDevice.shared.disconnectBLEDevice()
    }*/
    
    func disconnected() {        
        //blur background
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.regular)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.view.addSubview(blurEffectView)
        
        popUpView2.center = self.view.center
        popUpView2.layer.cornerRadius = 10
        self.view.addSubview(popUpView2)
        
        //force the user to push the btn
        tabBarController?.tabBar.isHidden = true
    }
}

//convert hex color #
extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(rgb: Int) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF
        )
    }
}
