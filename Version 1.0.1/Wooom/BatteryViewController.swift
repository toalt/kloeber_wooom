//
//  BatteryViewController.swift
//  Wooom
//
//  Created by Tobias Alt on 10.08.18.
//  Copyright © 2018 Kloeber. All rights reserved.
//

import UIKit
import WebKit

class BatteryViewController: UIViewController,WKUIDelegate {

    // Reference WKWebView
    var batteryWebView: WKWebView!
    
    // Notication Center for Update characteristics
    let nc = NotificationCenter.default
    
    // IBOutlets
    @IBOutlet weak var webContainer: UIView!
    @IBOutlet weak var progressInPercentage: UILabel!
    @IBOutlet weak var timeRemaining: UILabel!
    @IBOutlet weak var popUpView2: UIView!
    @IBOutlet weak var discBtn: UIButton!
    @IBOutlet weak var batteryImg: UIImageView!
    @IBOutlet var batteryProgress: UIView!
    
    // Batteryprogress as SubView
    let progressLayer = CAShapeLayer()
    let layerWidth = 74
    let layerHeight = 29
    
    //corparate color
    let primaryColor = UIColor(rgb: 0xFFBB02)
    
    // Create a global instance of NSUserDefaults class
    let preferences = UserDefaults.standard
    var language: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //get current language
        language = Locale.current.languageCode
        
        //only set height of the battery progress layer -> width will be dynamic below
        progressLayer.frame.size.height = CGFloat(layerHeight)
        progressLayer.backgroundColor = primaryColor.cgColor
        batteryProgress.layer.addSublayer(progressLayer)
        
        // WKWebView setup
        let webConfiguration = WKWebViewConfiguration()
        // Init WKWebView
        batteryWebView = WKWebView(frame: webContainer.bounds, configuration: webConfiguration)
        batteryWebView.uiDelegate = self
        // Add to container
        webContainer.addSubview(batteryWebView)
        
        // Constraints to fit container layout
        batteryWebView.translatesAutoresizingMaskIntoConstraints = false
        batteryWebView.leadingAnchor.constraint(equalTo: webContainer.leadingAnchor, constant: 0).isActive = true
        batteryWebView.trailingAnchor.constraint(equalTo: webContainer.trailingAnchor, constant: 0).isActive = true
        batteryWebView.topAnchor.constraint(equalTo: webContainer.topAnchor, constant: 0).isActive = true
        batteryWebView.bottomAnchor.constraint(equalTo: webContainer.bottomAnchor, constant: 0).isActive = true
        
        // Load WKWebView resources
        let htmlPath = Bundle.main.path(forResource: "battery", ofType: "html")
        let htmlUrl = URL(fileURLWithPath: htmlPath!, isDirectory: false)
        batteryWebView.loadFileURL(htmlUrl, allowingReadAccessTo: htmlUrl)
        // Let WebView appear transparent and fit full content
        batteryWebView.isOpaque = false
        batteryWebView.backgroundColor = UIColor.clear
        batteryWebView.scrollView.bounces = false
        batteryWebView.scrollView.contentInsetAdjustmentBehavior = .never
        
        // First notification observs is missed -> set voltage via singleton property
        if (BLEDevice.shared.isConnected == true) {
            switch (BLEDevice.shared.pwrSupplyState) {
                case 1:
                    showBatteryProgress()
                    if(BLEDevice.shared.voltage != nil) {
                        setBatteryProgress(value: BLEDevice.shared.voltage)
                        setBatteryText(value: BLEDevice.shared.voltage)
                    }
                    if((BLEDevice.shared.remainHours != nil) && (BLEDevice.shared.remainMin != nil)) {
                        setBatteryTimeRemaining(hours: BLEDevice.shared.remainHours, min: BLEDevice.shared.remainMin)
                    }
                    batteryImg.contentMode = UIView.ContentMode.scaleAspectFit
                    batteryImg.image = UIImage(named: "batteryOutline")
                    break
                case 2:
                    hideBatteryProgress()
                    if(BLEDevice.shared.voltage != nil) {
                        setBatteryText(value: BLEDevice.shared.voltage)
                    }
                    timeRemaining.text = ""
                    batteryImg.contentMode = UIView.ContentMode.scaleAspectFit
                    batteryImg.image = UIImage(named: "sparkMode")
                    break
                case 3:
                    hideBatteryProgress()
                    progressInPercentage.text = "––" + "%"
                    switch (language) {
                    case "de":
                        timeRemaining.text = "Kein Akku vorhanden"
                        break
                    case "en":
                        timeRemaining.text = "No battery available"
                        break
                    case "fr":
                        timeRemaining.text = "Pas de batterie disponible"
                        break
                    case "nl":
                        timeRemaining.text = "Geen batterij beschikbaar"
                        break
                    default:
                        timeRemaining.text = "No battery available"
                        break
                    }
                    timeRemaining.textColor = UIColor.white
                    batteryImg.contentMode = UIView.ContentMode.scaleAspectFit
                    batteryImg.image = UIImage(named: "noBattery")
                    break
                default:
                    break
            }
        }
        // Add Observer for listing to battery voltage characteristc
        nc.addObserver(self, selector: #selector(voltageDidUpdate(_:)), name: Notification.Name("VoltageUpdate"), object: nil)
        nc.addObserver(self, selector: #selector(remainDidUpdate(_:)), name: Notification.Name("TimeRemainUpdate"), object: nil)
        nc.addObserver(self, selector: #selector(lostConnection(_:)), name: Notification.Name("BLEDisconnected"), object: nil)
        nc.addObserver(self, selector: #selector(packState(_:)), name: Notification.Name("Pack"), object: nil)
        nc.addObserver(self, selector: #selector(bothState(_:)), name: Notification.Name("PackAndSpark"), object: nil)
        nc.addObserver(self, selector: #selector(sparkState(_:)), name: Notification.Name("Spark"), object: nil)
        
        discBtn.backgroundColor = primaryColor
        discBtn.layer.cornerRadius = 10

        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // Notification function called each time BLEDevice gets characteristc update
    @objc func voltageDidUpdate(_ notificaiton:Notification) {
        // Make sure that it is Akku Mode
        if(BLEDevice.shared.pwrSupplyState == 1) {
            setBatteryProgress(value: BLEDevice.shared.voltage)
            print("---- Voltage Called----",BLEDevice.shared.voltage)
            setBatteryText(value: BLEDevice.shared.voltage)
        }
        if(BLEDevice.shared.pwrSupplyState == 2) {
            setBatteryText(value: BLEDevice.shared.voltage)
        }
    }
    
    // Notification function called each time BLEDevice gets characteristc update
    @objc func remainDidUpdate(_ notificaiton:Notification) {
        // Make sure that it is Akku Mode
        if(BLEDevice.shared.pwrSupplyState == 1) {
            setBatteryTimeRemaining(hours: BLEDevice.shared.remainHours, min: BLEDevice.shared.remainMin)
        }
    }
    
    // Notification function called each time BLEDevice gets characteristc update
    @objc func packState(_ notificaiton:Notification) {
        showBatteryProgress()
        setBatteryProgress(value: BLEDevice.shared.voltage)
        setBatteryText(value: BLEDevice.shared.voltage)
        setBatteryTimeRemaining(hours: BLEDevice.shared.remainHours, min: BLEDevice.shared.remainMin)
        batteryImg.contentMode = UIView.ContentMode.scaleAspectFit
        batteryImg.image = UIImage(named: "batteryOutline")
    }
    
    // Notification function called each time BLEDevice gets characteristc update
    @objc func bothState(_ notificaiton:Notification) {
        hideBatteryProgress()
        setBatteryText(value: BLEDevice.shared.voltage)
        timeRemaining.text = ""
        batteryImg.contentMode = UIView.ContentMode.scaleAspectFit
        batteryImg.image = UIImage(named: "sparkMode")
    }
    
    // Notification function called each time BLEDevice gets characteristc update
    @objc func sparkState(_ notificaiton:Notification) {
        hideBatteryProgress()
        progressInPercentage.text = "––" + "%"
        switch (language) {
        case "de":
            timeRemaining.text = "Kein Akku vorhanden"
            break
        case "en":
            timeRemaining.text = "No battery available"
            break
        case "fr":
            timeRemaining.text = "Pas de batterie disponible"
            break
        case "nl":
            timeRemaining.text = "Geen batterij beschikbaar"
            break
        default:
            timeRemaining.text = "No battery available"
            break
        }
        timeRemaining.textColor = UIColor.white
        batteryImg.contentMode = UIView.ContentMode.scaleAspectFit
        batteryImg.image = UIImage(named: "noBattery")
    }
    
    func setBatteryProgress(value: Int) {
        let paramVoltage = (layerWidth * value) / 100
        
        if(paramVoltage <= 10) {
            progressLayer.frame.size.width = 10
            progressLayer.backgroundColor = UIColor.red.cgColor
        } else {
            progressLayer.frame.size.width = CGFloat(paramVoltage)
            progressLayer.backgroundColor = primaryColor.cgColor
        }
    }
    
    func setBatteryText(value: Int) {
        progressInPercentage.text = String(value) + "%"
        if(value <= 10) {
            progressInPercentage.text = "<10%"
        }
    }
    
    func hideBatteryProgress() {
        batteryProgress.isHidden = true
    }
    
    func showBatteryProgress() {
        batteryProgress.isHidden = false
    }
    
    func setBatteryTimeRemaining (hours: Int, min: Int) {
        if (BLEDevice.shared.voltage <= 10) {
            switch (language) {
            case "de":
                timeRemaining.text = "schwacher Batteriezustand, bitte aufladen"
                break
            case "en":
                timeRemaining.text = "Low battery, please recharge"
                break
            case "fr":
                timeRemaining.text = "Batterie faible, prière de recharger"
                break
            case "nl":
                timeRemaining.text = "Het oplaadniveau van de accu is te laag"
                break
            default:
                timeRemaining.text = "Low battery, please recharge"
                break
            }
            timeRemaining.textColor = UIColor.red
        } else {
            if (min < 10) {            switch (language) {
            case "de":
                timeRemaining.text = "Restlaufzeit beträgt \(hours) h 0\(min) min"
                break
            case "en":
                timeRemaining.text = "Time left \(hours) h 0\(min) min"
                break
            case "fr":
                timeRemaining.text = "Le temps restant est de \(hours) h 0\(min) min"
                break
            case "nl":
                timeRemaining.text = "Resterende looptijd bedraagt \(hours) h 0\(min) min"
                break
            default:
                timeRemaining.text = "Time left \(hours) h 0\(min) min"
                break
                }
            } else {
                switch (language) {
                case "de":
                    timeRemaining.text = "Restlaufzeit beträgt \(hours) h \(min) min"
                    break
                case "en":
                    timeRemaining.text = "Time left \(hours) h \(min) min"
                    break
                case "fr":
                    timeRemaining.text = "Le temps restant est de \(hours) h \(min) min"
                    break
                case "nl":
                    timeRemaining.text = "Resterende looptijd bedraagt \(hours) h \(min) min"
                    break
                default:
                    timeRemaining.text = "Time left \(hours) h \(min) min"
                    break
                }
            }
            timeRemaining.textColor = UIColor.white
        }
    }
    
    // Notification function called when device gets disconnected
    @objc func lostConnection(_ notificaiton:Notification) {
        disconnected()
    }
    
    func disconnected() {
        //blur background
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.regular)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.view.addSubview(blurEffectView)
        
        popUpView2.center = self.view.center
        popUpView2.layer.cornerRadius = 10
        self.view.addSubview(popUpView2)
        
        tabBarController?.tabBar.isHidden = true
    }
    
 /*   override func viewDidAppear(_ animated: Bool) {
        setBatteryProgress(value: BLEDevice.shared.voltage)
    } */
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
