var chair = document.getElementById("chair");
var modusBar = document.getElementById("modus");
var buttonOne = document.getElementById("btnOne");
var buttonTwo = document.getElementById("btnTwo");
var buttonThree = document.getElementById("btnThree");
var buttonFour = document.getElementById("btnFour");

var rippleTop = document.getElementById("circleTop");
var rippleMid = document.getElementById("circleMid");
var rippleBot = document.getElementById("circleBot");

var coreTop = document.getElementById("coreTop");
var outerTop = document.getElementById("outerTop");
var coreMid = document.getElementById("coreMid");
var outerMid = document.getElementById("outerMid");
var coreBot = document.getElementById("coreBot");
var outerBot = document.getElementById("outerBot");

//media queries
var iPhonePlus = window.matchMedia("(min-height: 400px)");
var iPhoneX = window.matchMedia("(min-height: 420px)");
var iPhoneMax = window.matchMedia("(min-height: 500px)");
var iPadNineInch = window.matchMedia("(min-height: 650px)");
var iPadTenInch = window.matchMedia("(min-height: 750px)");
var iPadElevenInch = window.matchMedia("(min-height: 830px)");
var iPadTwelveInch = window.matchMedia("(min-height: 990px)");

function enableModus() {
    modusBar.style.opacity = 0.9;
    chair.style.opacity = 0.9;
    buttonOne.style.pointerEvents = "auto";
    buttonTwo.style.pointerEvents = "auto";
    buttonThree.style.pointerEvents = "auto";
    buttonFour.style.pointerEvents = "auto";
    
    stageZeroTop();
    stageZeroMid();
    stageZeroBot();
    //rippleTop.innerHTML = document.body.clientHeight;
}

function disableModus() {
    modusBar.style.opacity = 0.3;
    chair.style.opacity = 0.3;
    buttonOne.style.pointerEvents = "none";
    buttonTwo.style.pointerEvents = "none";
    buttonThree.style.pointerEvents = "none";
    buttonFour.style.pointerEvents = "none";
    
    rippleTop.style.opacity = 0.4;
    rippleTop.style.top = "10%";
    if (iPhoneX.matches) {
        rippleTop.style.top = "13%";
    }
    if (iPhoneMax.matches) {
        rippleTop.style.top = "18%";
    }
    if (iPadNineInch.matches) {
        rippleTop.style.top = "12%";
    }
    if (iPadTenInch.matches) {
        rippleTop.style.top = "14%";
    }
    if (iPadElevenInch.matches) {
        rippleTop.style.top = "16%";
    }
    rippleTop.style.width = "56px";
    rippleTop.style.height = "56px";
    rippleTop.style.marginLeft = "-28px";
    rippleTop.style.background = "rgba(255,255,255,0.3)";
    rippleTop.style.animation = "";
    outerTop.style.opacity = 1.0;
    coreTop.style.opacity = 1.0;
    
    rippleMid.style.opacity = 0.4;
    rippleMid.style.top = "27%";
    if (iPhoneX.matches) {
        rippleMid.style.top = "30%";
    }
    if (iPhoneMax.matches) {
        rippleMid.style.top = "32%";
    }
    if (iPadNineInch.matches) {
        rippleMid.style.top = "28%";
    }
    if (iPadTenInch.matches) {
        rippleMid.style.top = "28%";
    }
    if (iPadElevenInch.matches) {
        rippleMid.style.top = "30%";
    }
    rippleMid.style.width = "56px";
    rippleMid.style.height = "56px";
    rippleMid.style.marginLeft = "-28px";
    rippleMid.style.background = "rgba(255,255,255,0.3)";
    rippleMid.style.animation = "";
    outerMid.style.opacity = 1.0;
    coreMid.style.opacity = 1.0;
    
    rippleBot.style.opacity = 0.4;
    rippleBot.style.top = "44%";
    if (iPhoneX.matches) {
        rippleBot.style.top = "47%";
    }
    if (iPhoneMax.matches) {
        rippleBot.style.top = "46%";
    }
    if (iPadNineInch.matches) {
        rippleBot.style.top = "44%";
    }
    if (iPadTenInch.matches) {
        rippleBot.style.top = "42%";
    }
    if (iPadElevenInch.matches) {
        rippleBot.style.top = "44%";
    }
    rippleBot.style.width = "56px";
    rippleBot.style.height = "56px";
    rippleBot.style.marginLeft = "-28px";
    rippleBot.style.background = "rgba(255,255,255,0.3)";
    rippleBot.style.animation = "";
    outerBot.style.opacity = 1.0;
    coreBot.style.opacity = 1.0;
}

function setModus(modus) {
    
    switch (modus) {
        case "one":
            buttonOne.style.background = "#FFBB02";
            buttonTwo.style.background = "white";
            buttonThree.style.background = "white";
            buttonFour.style.background = "white";
            //send to native controller
            window.webkit.messageHandlers.modusOne.postMessage("Modus1");
            break;
        case "two":
            buttonOne.style.background = "white";
            buttonTwo.style.background = "#FFBB02";
            buttonThree.style.background = "white";
            buttonFour.style.background = "white";
            window.webkit.messageHandlers.modusTwo.postMessage("Modus2");
            break;
        case "three":
            buttonOne.style.background = "white";
            buttonTwo.style.background = "white";
            buttonThree.style.background = "#FFBB02";
            buttonFour.style.background = "white";
            window.webkit.messageHandlers.modusThree.postMessage("Modus3");
            break;
        case "four":
            buttonOne.style.background = "white";
            buttonTwo.style.background = "white";
            buttonThree.style.background = "white";
            buttonFour.style.background = "#FFBB02";
            window.webkit.messageHandlers.modusFour.postMessage("Modus4");
            break;
    }
}

function stageZeroTop() {
    rippleTop.style.opacity = 1.0;
    rippleTop.style.top = "10%";
    if (iPhoneX.matches) {
        rippleTop.style.top = "13%";
    }
    if (iPhoneMax.matches) {
        rippleTop.style.top = "18%";
    }
    if (iPadNineInch.matches) {
        rippleTop.style.top = "12%";
    }
    if (iPadTenInch.matches) {
        rippleTop.style.top = "14%";
    }
    if (iPadElevenInch.matches) {
        rippleTop.style.top = "16%";
    }
    rippleTop.style.width = "56px";
    rippleTop.style.height = "56px";
    rippleTop.style.marginLeft = "-28px";
    rippleTop.style.background = "rgba(255,255,255,0.3)";
    rippleTop.style.animation = "";
    outerTop.style.opacity = 1.0;
    coreTop.style.opacity = 1.0;
}

function stageOneTop() {
    rippleTop.style.width = "6px";
    rippleTop.style.height = "6px";
    rippleTop.style.marginLeft = "-3px";
    //window iPhone / iPad
    rippleTop.style.top = "17%";
    if (iPhonePlus.matches) {
        rippleTop.style.top = "16%";
    }
    if (iPhoneX.matches) {
        rippleTop.style.top = "19%";
    }
    if (iPhoneMax.matches) {
        rippleTop.style.top = "23%";
    }
    if (iPadNineInch.matches) {
        rippleTop.style.top = "16%";
    }
    if (iPadTenInch.matches) {
        rippleTop.style.top = "17%";
    }
    if (iPadElevenInch.matches) {
        rippleTop.style.top = "19%";
    }
    if (iPadTwelveInch.matches) {
        rippleTop.style.top = "18.5%";
    }
    rippleTop.style.background = "transparent";
    rippleTop.style.animation = "rippleOne 1.2s linear infinite";
    outerTop.style.opacity = 0;
    coreTop.style.opacity = 0;
}

function stageTwoTop() {
    rippleTop.style.width = "6px";
    rippleTop.style.height = "6px";
    rippleTop.style.marginLeft = "-3px";
    //window iPhoneX
    rippleTop.style.top = "17%";
    if (iPhonePlus.matches) {
        rippleTop.style.top = "16%";
    }
    if (iPhoneX.matches) {
        rippleTop.style.top = "19%";
    }
    if (iPhoneMax.matches) {
        rippleTop.style.top = "23%";
    }
    if (iPadNineInch.matches) {
        rippleTop.style.top = "16%";
    }
    if (iPadTenInch.matches) {
        rippleTop.style.top = "17%";
    }
    if (iPadElevenInch.matches) {
        rippleTop.style.top = "19%";
    }
    if (iPadTwelveInch.matches) {
        rippleTop.style.top = "18.5%";
    }
    rippleTop.style.background = "transparent";
    rippleTop.style.animation = "rippleTwo 1.0s linear infinite";
    outerTop.style.opacity = 0;
    coreTop.style.opacity = 0;
}

function stageThreeTop() {
    rippleTop.style.width = "6px";
    rippleTop.style.height = "6px";
    rippleTop.style.marginLeft = "-3px";
    //window iPhoneX
    rippleTop.style.top = "17%";
    if (iPhonePlus.matches) {
        rippleTop.style.top = "16%";
    }
    if (iPhoneX.matches) {
        rippleTop.style.top = "19%";
    }
    if (iPhoneMax.matches) {
        rippleTop.style.top = "23%";
    }
    if (iPadNineInch.matches) {
        rippleTop.style.top = "16%";
    }
    if (iPadTenInch.matches) {
        rippleTop.style.top = "17%";
    }
    if (iPadElevenInch.matches) {
        rippleTop.style.top = "19%";
    }
    if (iPadTwelveInch.matches) {
        rippleTop.style.top = "18.5%";
    }
    rippleTop.style.background = "transparent";
    rippleTop.style.animation = "rippleThree 0.8s linear infinite";
    outerTop.style.opacity = 0;
    coreTop.style.opacity = 0;
}

function stageFourTop() {
    rippleTop.style.width = "6px";
    rippleTop.style.height = "6px";
    rippleTop.style.marginLeft = "-3px";
    //window iPhoneX
    rippleTop.style.top = "17%";
    if (iPhonePlus.matches) {
        rippleTop.style.top = "16%";
    }
    if (iPhoneX.matches) {
        rippleTop.style.top = "19%";
    }
    if (iPhoneMax.matches) {
        rippleTop.style.top = "23%";
    }
    if (iPadNineInch.matches) {
        rippleTop.style.top = "16%";
    }
    if (iPadTenInch.matches) {
        rippleTop.style.top = "17%";
    }
    if (iPadElevenInch.matches) {
        rippleTop.style.top = "19%";
    }
    if (iPadTwelveInch.matches) {
        rippleTop.style.top = "18.5%";
    }
    rippleTop.style.background = "transparent";
    rippleTop.style.animation = "rippleFour 0.6s linear infinite";
    outerTop.style.opacity = 0;
    coreTop.style.opacity = 0;
}

function stageZeroMid() {
    rippleMid.style.opacity = 1.0;
    rippleMid.style.top = "27%";
    if (iPhoneX.matches) {
        rippleMid.style.top = "30%";
    }
    if (iPhoneMax.matches) {
        rippleMid.style.top = "32%";
    }
    if (iPadNineInch.matches) {
        rippleMid.style.top = "28%";
    }
    if (iPadTenInch.matches) {
        rippleMid.style.top = "28%";
    }
    if (iPadElevenInch.matches) {
        rippleMid.style.top = "30%";
    }
    rippleMid.style.width = "56px";
    rippleMid.style.height = "56px";
    rippleMid.style.marginLeft = "-28px";
    rippleMid.style.background = "rgba(255,255,255,0.3)";
    rippleMid.style.animation = "";
    outerMid.style.opacity = 1.0;
    coreMid.style.opacity = 1.0;
}

function stageOneMid() {
    rippleMid.style.width = "6px";
    rippleMid.style.height = "6px";
    rippleMid.style.marginLeft = "-3px";
    //window iPhoneX
    rippleMid.style.top = "34.5%";
    if (iPhonePlus.matches) {
        rippleMid.style.top = "33%";
    }
    if (iPhoneX.matches) {
        rippleMid.style.top = "36%";
    }
    if (iPhoneMax.matches) {
        rippleMid.style.top = "37%";
    }
    if (iPadNineInch.matches) {
        rippleMid.style.top = "32%";
    }
    if (iPadTenInch.matches) {
        rippleMid.style.top = "31%";
    }
    if (iPadElevenInch.matches) {
        rippleMid.style.top = "33%";
    }
    if (iPadTwelveInch.matches) {
        rippleMid.style.top = "32.5%";
    }
    rippleMid.style.background = "transparent";
    rippleMid.style.animation = "rippleOne 1.2s linear infinite";
    outerMid.style.opacity = 0;
    coreMid.style.opacity = 0;
}

function stageTwoMid() {
    rippleMid.style.width = "6px";
    rippleMid.style.height = "6px";
    rippleMid.style.marginLeft = "-3px";
    //window iPhoneX
    rippleMid.style.top = "34.5%";
    if (iPhonePlus.matches) {
        rippleMid.style.top = "33%";
    }
    if (iPhoneX.matches) {
        rippleMid.style.top = "36%";
    }
    if (iPhoneMax.matches) {
        rippleMid.style.top = "37%";
    }
    if (iPadNineInch.matches) {
        rippleMid.style.top = "32%";
    }
    if (iPadTenInch.matches) {
        rippleMid.style.top = "31%";
    }
    if (iPadElevenInch.matches) {
        rippleMid.style.top = "33%";
    }
    if (iPadTwelveInch.matches) {
        rippleMid.style.top = "32.5%";
    }
    rippleMid.style.background = "transparent";
    rippleMid.style.animation = "rippleTwo 1.0s linear infinite";
    outerMid.style.opacity = 0;
    coreMid.style.opacity = 0;
}

function stageThreeMid() {
    rippleMid.style.width = "6px";
    rippleMid.style.height = "6px";
    rippleMid.style.marginLeft = "-3px";
    //window iPhoneX
    rippleMid.style.top = "34.5%";
    if (iPhonePlus.matches) {
        rippleMid.style.top = "33%";
    }
    if (iPhoneX.matches) {
        rippleMid.style.top = "36%";
    }
    if (iPhoneMax.matches) {
        rippleMid.style.top = "37%";
    }
    if (iPadNineInch.matches) {
        rippleMid.style.top = "32%";
    }
    if (iPadTenInch.matches) {
        rippleMid.style.top = "31%";
    }
    if (iPadElevenInch.matches) {
        rippleMid.style.top = "33%";
    }
    if (iPadTwelveInch.matches) {
        rippleMid.style.top = "32.5%";
    }
    rippleMid.style.background = "transparent";
    rippleMid.style.animation = "rippleThree 0.8s linear infinite";
    outerMid.style.opacity = 0;
    coreMid.style.opacity = 0;
}

function stageFourMid() {
    rippleMid.style.width = "6px";
    rippleMid.style.height = "6px";
    rippleMid.style.marginLeft = "-3px";
    //window iPhoneX
    rippleMid.style.top = "34.5%";
    if (iPhonePlus.matches) {
        rippleMid.style.top = "33%";
    }
    if (iPhoneX.matches) {
        rippleMid.style.top = "36%";
    }
    if (iPhoneMax.matches) {
        rippleMid.style.top = "37%";
    }
    if (iPadNineInch.matches) {
        rippleMid.style.top = "32%";
    }
    if (iPadTenInch.matches) {
        rippleMid.style.top = "31%";
    }
    if (iPadElevenInch.matches) {
        rippleMid.style.top = "33%";
    }
    if (iPadTwelveInch.matches) {
        rippleMid.style.top = "32.5%";
    }
    rippleMid.style.background = "transparent";
    rippleMid.style.animation = "rippleFour 0.6s linear infinite";
    outerMid.style.opacity = 0;
    coreMid.style.opacity = 0;
}

function stageZeroBot() {
    rippleBot.style.opacity = 1.0;
    rippleBot.style.top = "44%";
    if (iPhoneX.matches) {
        rippleBot.style.top = "47%";
    }
    if (iPhoneMax.matches) {
        rippleBot.style.top = "46%";
    }
    if (iPadNineInch.matches) {
        rippleBot.style.top = "44%";
    }
    if (iPadTenInch.matches) {
        rippleBot.style.top = "42%";
    }
    if (iPadElevenInch.matches) {
        rippleBot.style.top = "44%";
    }
    rippleBot.style.width = "56px";
    rippleBot.style.height = "56px";
    rippleBot.style.marginLeft = "-28px";
    rippleBot.style.background = "rgba(255,255,255,0.3)";
    rippleBot.style.animation = "";
    outerBot.style.opacity = 1.0;
    coreBot.style.opacity = 1.0;
}

function stageOneBot() {
    rippleBot.style.width = "6px";
    rippleBot.style.height = "6px";
    rippleBot.style.marginLeft = "-3px";
    //window iPhoneX
    rippleBot.style.top = "52%";
    if (iPhonePlus.matches) {
        rippleBot.style.top = "50%";
    }
    if (iPhoneX.matches) {
        rippleBot.style.top = "53%";
    }
    if (iPhoneMax.matches) {
        rippleBot.style.top = "51%";
    }
    if (iPadNineInch.matches) {
        rippleBot.style.top = "48%";
    }
    if (iPadTenInch.matches) {
        rippleBot.style.top = "45%";
    }
    if (iPadElevenInch.matches) {
        rippleBot.style.top = "47%";
    }
    if (iPadTwelveInch.matches) {
        rippleBot.style.top = "46.5%";
    }
    rippleBot.style.background = "transparent";
    rippleBot.style.animation = "rippleOne 1.2s linear infinite";
    outerBot.style.opacity = 0;
    coreBot.style.opacity = 0;
}

function stageTwoBot() {
    rippleBot.style.width = "6px";
    rippleBot.style.height = "6px";
    rippleBot.style.marginLeft = "-3px";
    //window iPhoneX
    rippleBot.style.top = "52%";
    if (iPhonePlus.matches) {
        rippleBot.style.top = "50%";
    }
    if (iPhoneX.matches) {
        rippleBot.style.top = "53%";
    }
    if (iPhoneMax.matches) {
        rippleBot.style.top = "51%";
    }
    if (iPadNineInch.matches) {
        rippleBot.style.top = "48%";
    }
    if (iPadTenInch.matches) {
        rippleBot.style.top = "45%";
    }
    if (iPadElevenInch.matches) {
        rippleBot.style.top = "47%";
    }
    if (iPadTwelveInch.matches) {
        rippleBot.style.top = "46.5%";
    }
    rippleBot.style.background = "transparent";
    rippleBot.style.animation = "rippleTwo 1.0s linear infinite";
    outerBot.style.opacity = 0;
    coreBot.style.opacity = 0;
}

function stageThreeBot() {
    rippleBot.style.width = "6px";
    rippleBot.style.height = "6px";
    rippleBot.style.marginLeft = "-3px";
    //window iPhoneX
    rippleBot.style.top = "52%";
    if (iPhonePlus.matches) {
        rippleBot.style.top = "50%";
    }
    if (iPhoneX.matches) {
        rippleBot.style.top = "53%";
    }
    if (iPhoneMax.matches) {
        rippleBot.style.top = "51%";
    }
    if (iPadNineInch.matches) {
        rippleBot.style.top = "48%";
    }
    if (iPadTenInch.matches) {
        rippleBot.style.top = "45%";
    }
    if (iPadElevenInch.matches) {
        rippleBot.style.top = "47%";
    }
    if (iPadTwelveInch.matches) {
        rippleBot.style.top = "46.5%";
    }
    rippleBot.style.background = "transparent";
    rippleBot.style.animation = "rippleThree 0.8s linear infinite";
    outerBot.style.opacity = 0;
    coreBot.style.opacity = 0;
}

function stageFourBot() {
    rippleBot.style.width = "6px";
    rippleBot.style.height = "6px";
    rippleBot.style.marginLeft = "-3px";
    //window iPhoneX
    rippleBot.style.top = "52%";
    if (iPhonePlus.matches) {
        rippleBot.style.top = "50%";
    }
    if (iPhoneX.matches) {
        rippleBot.style.top = "53%";
    }
    if (iPhoneMax.matches) {
        rippleBot.style.top = "51%";
    }
    if (iPadNineInch.matches) {
        rippleBot.style.top = "48%";
    }
    if (iPadTenInch.matches) {
        rippleBot.style.top = "45%";
    }
    if (iPadElevenInch.matches) {
        rippleBot.style.top = "47%";
    }
    if (iPadTwelveInch.matches) {
        rippleBot.style.top = "46.5%";
    }
    rippleBot.style.background = "transparent";
    rippleBot.style.animation = "rippleFour 0.6s linear infinite";
    outerBot.style.opacity = 0;
    coreBot.style.opacity = 0;
}

