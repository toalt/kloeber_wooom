//
//  SettingsViewController.swift
//  Wooom
//
//  Created by Tobias Alt on 10.08.18.
//  Copyright © 2018 Kloeber. All rights reserved.
//

import UIKit


class SettingsViewController: UIViewController {
    
    // IBOutlets
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var connectinState: UILabel!
    @IBOutlet weak var stateView: UIView!
    @IBOutlet weak var popUpView: UIView!
    @IBOutlet weak var reconBtn: UIButton!
    @IBOutlet weak var popUpView2: UIView!
    @IBOutlet weak var discBtn: UIButton!
    
    // Create a global instance of NSUserDefaults class
    let preferences = UserDefaults.standard
    
    // Notication Center for update device state
    let nc = NotificationCenter.default
    
    //corparate color
    let primaryColor = UIColor(rgb: 0xFFBB02)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Add onclick event to each view
        let tapGestureRecognizerState = UITapGestureRecognizer(target: self, action: #selector(onStateTapped(gestureRecognizer:)))
        stateView.addGestureRecognizer(tapGestureRecognizerState)
        
        // Add Observer for listing to sensor state
        //nc.addObserver(self, selector: #selector(sensorStateChanged(_:)), name: Notification.Name("SensorEvent"), object: nil)
        nc.addObserver(self, selector: #selector(lostConnection(_:)), name: Notification.Name("BLEDisconnected"), object: nil)
        
        // Style for blur effect when disconnected
        reconBtn.backgroundColor = primaryColor
        reconBtn.layer.cornerRadius = 10
        
        discBtn.backgroundColor = primaryColor
        discBtn.layer.cornerRadius = 10
        
        // write ID to settings label 
        setId()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @objc func onStateTapped(gestureRecognizer: UIGestureRecognizer) {
        self.performSegue(withIdentifier: "showConState", sender: nil)
    }
    
    func setId() {
        if(BLEDevice.shared.isConnected == true) {
            // fetch the preferences
            let storedDeviceName = preferences.object(forKey: "pairedDevice") as! String
            connectinState.text = "Stuhl verbunden"
            idLabel.text = storedDeviceName
        } else {
            connectinState.text = "Stuhl nicht verbunden"
        }
    }
    
    // Notification function called when device is connected
    /*@objc func sensorStateChanged(_ notificaiton:Notification) {
        //check sensor state
        if (BLEDevice.shared.sensorState == 0) {
            disconnectedFromSensor()
        }
    }*/
    
    // Notification function called when device gets disconnected
    @objc func lostConnection(_ notificaiton:Notification) {
        disconnected()
    }
    
    func disconnected() {
        //blur background
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.regular)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.view.addSubview(blurEffectView)
        
        popUpView2.center = self.view.center
        popUpView2.layer.cornerRadius = 10
        self.view.addSubview(popUpView2)
        
        tabBarController?.tabBar.isHidden = true
        
    }
    
    /*func disconnectedFromSensor() {
        //blur background
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.regular)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.view.addSubview(blurEffectView)
        
        popUpView.center = self.view.center
        popUpView.layer.cornerRadius = 10
        self.view.addSubview(popUpView)
        
        // force the user to push the btn
        tabBarController?.tabBar.isHidden = true
        // disconnect the device
        BLEDevice.shared.disconnectBLEDevice()
    } */
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}


