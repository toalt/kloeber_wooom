//
//  SecondViewController.swift
//  Wooom
//
//  Created by Tobias Alt on 07.08.18.
//  Copyright © 2018 Kloeber. All rights reserved.
//

import UIKit
import WebKit

class MassageViewController: UIViewController,WKUIDelegate,WKNavigationDelegate,WKScriptMessageHandler {

    // WKWebView for chair SVG
    var massageWebView: WKWebView!
    
    // IBOutlets
    @IBOutlet weak var switchState: UISwitch!
    @IBOutlet weak var webContainer: UIView!
    @IBOutlet weak var topSlider: UISlider!
    @IBOutlet weak var midSlider: UISlider!
    @IBOutlet weak var botSlider: UISlider!
    @IBOutlet weak var popUpView: UIView!
    @IBOutlet weak var reconBtn: UIButton!
    @IBOutlet weak var popUpView2: UIView!
    @IBOutlet weak var discBtn: UIButton!
    
    // Corparate color
    let primaryColor = UIColor(rgb: 0xFFBB02)
    
    // Last modus from user saved
    var lastModus:String = "one";
    //var lastTopValue =
    //var lastMidValue =
    //var lastBotValue =
    
    // Notication Center for update device state
    let nc = NotificationCenter.default
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Color the switch background
        switchState.backgroundColor = UIColor.gray
        switchState.layer.cornerRadius = 16.0
        
        // Add a gesture recognizer to the slider
        let tapGestureRecognizerTop = UITapGestureRecognizer(target: self, action: #selector(topSliderTapped(gestureRecognizer:)))
        topSlider.addGestureRecognizer(tapGestureRecognizerTop)
        let tapGestureRecognizerMid = UITapGestureRecognizer(target: self, action: #selector(midSliderTapped(gestureRecognizer:)))
        midSlider.addGestureRecognizer(tapGestureRecognizerMid)
        let tapGestureRecognizerBot = UITapGestureRecognizer(target: self, action: #selector(botSliderTapped(gestureRecognizer:)))
        botSlider.addGestureRecognizer(tapGestureRecognizerBot)
        
        // Image for slider thumb
        topSlider.setThumbImage(#imageLiteral(resourceName: "thumb"), for: .normal)
        midSlider.setThumbImage(#imageLiteral(resourceName: "thumb"), for: .normal)
        botSlider.setThumbImage(#imageLiteral(resourceName: "thumb"), for: .normal)
        
        // WKWebView setup
        let webConfiguration = WKWebViewConfiguration()
        let contentController = WKUserContentController()
        //message name for js bridge
        contentController.add(self, name: "modusOne")
        contentController.add(self, name: "modusTwo")
        contentController.add(self, name: "modusThree")
        contentController.add(self, name: "modusFour")
        webConfiguration.userContentController = contentController
        // Init WKWebView
        massageWebView = WKWebView(frame: webContainer.bounds, configuration: webConfiguration)
        massageWebView.uiDelegate = self
        massageWebView.navigationDelegate = self
        // Add to container
        webContainer.addSubview(massageWebView)
        
        // Constraints to fit container layout
        massageWebView.translatesAutoresizingMaskIntoConstraints = false
        massageWebView.leadingAnchor.constraint(equalTo: webContainer.leadingAnchor, constant: 0).isActive = true
        massageWebView.trailingAnchor.constraint(equalTo: webContainer.trailingAnchor, constant: 0).isActive = true
        massageWebView.topAnchor.constraint(equalTo: webContainer.topAnchor, constant: 0).isActive = true
        massageWebView.bottomAnchor.constraint(equalTo: webContainer.bottomAnchor, constant: 0).isActive = true
        
        // Load WKWebView resources
        let htmlPath = Bundle.main.path(forResource: "massage", ofType: "html")
        let htmlUrl = URL(fileURLWithPath: htmlPath!, isDirectory: false)
        massageWebView.loadFileURL(htmlUrl, allowingReadAccessTo: htmlUrl)
        // Let WebView appear transparent and fit full content
        massageWebView.isOpaque = false
        massageWebView.backgroundColor = UIColor.clear
        massageWebView.scrollView.bounces = false
        massageWebView.scrollView.contentInsetAdjustmentBehavior = .never
        
        // Add Observer for listing to sensor state
        //nc.addObserver(self, selector: #selector(sensorStateChanged(_:)), name: Notification.Name("SensorEvent"), object: nil)
        nc.addObserver(self, selector: #selector(lostConnection(_:)), name: Notification.Name("BLEDisconnected"), object: nil)
        
        // Style for blur effect when disconnected
        reconBtn.backgroundColor = primaryColor
        reconBtn.layer.cornerRadius = 10
        
        discBtn.backgroundColor = primaryColor
        discBtn.layer.cornerRadius = 10
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        //load the last user state
        //print("----Lade letzte Werte ------")
        /*massageWebView.evaluateJavaScript("setModus('\(lastModus)');")
        topSlider.value = 56
        midSlider.value = 15
        botSlider.value = 88 */
        //initial modus state is 1
        massageWebView.evaluateJavaScript("setModus('\("one")');")
    }
    
    // JS Bridge Observer for messages -> name is identifier not message
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        if (message.name == "modusOne") {
            BLEDevice.shared.writeMassageModusValue(value: 1)
        }
        if (message.name == "modusTwo") {
            BLEDevice.shared.writeMassageModusValue(value: 2)
        }
        if (message.name == "modusThree") {
            BLEDevice.shared.writeMassageModusValue(value: 3)
        }
        if (message.name == "modusFour") {
            BLEDevice.shared.writeMassageModusValue(value: 4)
        }
    }
    
    // Switch Slider to en-/disable whole tab
    @IBAction func currentState(_ sender: Any) {
        if switchState.isOn {
            topSlider.isEnabled = true
            midSlider.isEnabled = true
            botSlider.isEnabled = true
            massageWebView.evaluateJavaScript("enableModus();")
            
            // Load last values from BLE board
        } else {
            topSlider.isEnabled = false
            midSlider.isEnabled = false
            botSlider.isEnabled = false
            massageWebView.evaluateJavaScript("disableModus();")
            // Reset Massage values on BLE board
            BLEDevice.shared.disableMassageTab()
            // Reset GUI values to 0
            topSlider.value = 0
            midSlider.value = 0
            botSlider.value = 0
        }
    }
    
    @IBAction func topSliderChanged(_ sender: Any) {
        let intValueTop:Int = Int(topSlider.value)
        print ("Rückenbereich ist auf \(intValueTop)")
        BLEDevice.shared.writeMassageTopValue(value: intValueTop)
    }
    
    @objc func topSliderTapped(gestureRecognizer: UIGestureRecognizer) {
        let pointTapped: CGPoint = gestureRecognizer.location(in: self.view)
        let positionOfSlider: CGPoint = topSlider.frame.origin
        let widthOfSlider: CGFloat = topSlider.frame.size.width
        let newValue = ((pointTapped.x - positionOfSlider.x) * CGFloat(topSlider.maximumValue) / widthOfSlider)
        topSlider.setValue(Float(newValue), animated: true)
        
        // Same arg as in value changed event -> BLE value
        let intValueTop:NSInteger = NSInteger(topSlider.value)
        print ("Massage oben ist auf \(intValueTop)")
        BLEDevice.shared.writeMassageTopValue(value: intValueTop)
    }
    
    @IBAction func midSliderChanged(_ sender: Any) {
        let intValueMid:Int = Int(midSlider.value)
        print ("Lendenbereich ist auf \(intValueMid)")
        BLEDevice.shared.writeMassageMidValue(value: intValueMid)
    }
    
    @objc func midSliderTapped(gestureRecognizer: UIGestureRecognizer) {
        let pointTapped: CGPoint = gestureRecognizer.location(in: self.view)
        let positionOfSlider: CGPoint = midSlider.frame.origin
        let widthOfSlider: CGFloat = midSlider.frame.size.width
        let newValue = ((pointTapped.x - positionOfSlider.x) * CGFloat(midSlider.maximumValue) / widthOfSlider)
        midSlider.setValue(Float(newValue), animated: true)
        
        // Same arg as in value changed event -> BLE value
        let intValueMid:NSInteger = NSInteger(midSlider.value)
        print ("Massage mittig ist auf \(intValueMid)")
        BLEDevice.shared.writeMassageMidValue(value: intValueMid)
    }
    
    @IBAction func botSliderChanged(_ sender: Any) {
        let intValueBot:Int = Int(botSlider.value)
        print ("Sitzfläche ist auf \(intValueBot)")
        BLEDevice.shared.writeMassageBotValue(value: intValueBot)
    }
    
    @objc func botSliderTapped(gestureRecognizer: UIGestureRecognizer) {
        let pointTapped: CGPoint = gestureRecognizer.location(in: self.view)
        let positionOfSlider: CGPoint = botSlider.frame.origin
        let widthOfSlider: CGFloat = botSlider.frame.size.width
        let newValue = ((pointTapped.x - positionOfSlider.x) * CGFloat(botSlider.maximumValue) / widthOfSlider)
        botSlider.setValue(Float(newValue), animated: true)
        
        // Same arg as in value changed event -> BLE value
        let intValueBot:NSInteger = NSInteger(botSlider.value)
        print ("Message unten ist auf \(intValueBot)")
        BLEDevice.shared.writeMassageBotValue(value: intValueBot)
    }
    
    // Notification function called when device is connected
    /*@objc func sensorStateChanged(_ notificaiton:Notification) {
        //check sensor state
        if (BLEDevice.shared.sensorState == 0) {
            disconnectedFromSensor()
        }
    }*/
    
    // Notification function called when device gets disconnected
    @objc func lostConnection(_ notificaiton:Notification) {
        disconnected()
    }
    
    /*func disconnectedFromSensor() {
        //blur background
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.regular)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.view.addSubview(blurEffectView)
        
        popUpView.center = self.view.center
        popUpView.layer.cornerRadius = 10
        self.view.addSubview(popUpView)
        
        // force the user to push the btn
        tabBarController?.tabBar.isHidden = true
        // disconnect the device
        BLEDevice.shared.disconnectBLEDevice()
    }*/
    
    func disconnected() {
        //blur background
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.regular)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.view.addSubview(blurEffectView)
        
        popUpView2.center = self.view.center
        popUpView2.layer.cornerRadius = 10
        self.view.addSubview(popUpView2)
        
        tabBarController?.tabBar.isHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

