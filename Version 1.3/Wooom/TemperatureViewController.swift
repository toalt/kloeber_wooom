//
//  TemperatureViewController.swift
//  Wooom
//
//  Created by Tobias Alt on 10.08.18.
//  Copyright © 2018 Kloeber. All rights reserved.
//

import UIKit
import WebKit

class TemperatureViewController: UIViewController,WKUIDelegate,WKScriptMessageHandler {

    // WKWebView for chair SVG
    var tempWebView: WKWebView!
    
    // IBOutlets
    @IBOutlet weak var switchState: UISwitch!
    @IBOutlet weak var webContainer: UIView!
    @IBOutlet weak var sitSlider: UISlider!
    @IBOutlet weak var sitLabel: UILabel!
    @IBOutlet weak var backSlider: UISlider!
    @IBOutlet weak var backLabel: UILabel!
    @IBOutlet weak var discBtn: UIButton!
    
    @IBOutlet weak var popUpView2: UIView!
    
    //corparate color
    let primaryColor = UIColor(rgb: 0xFFBB02)
    
    // Notication Center for update device state
    let nc = NotificationCenter.default
    
    // Create a global instance of NSUserDefaults class
    let preferences = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Change background color of the switch
        switchState.backgroundColor = UIColor.gray
        switchState.layer.cornerRadius = 16.0
        
        // Add a gesture recognizer to the slider
        let tapGestureRecognizerSit = UITapGestureRecognizer(target: self, action: #selector(sitSliderTapped(gestureRecognizer:)))
        sitSlider.addGestureRecognizer(tapGestureRecognizerSit)
        let tapGestureRecognizerBack = UITapGestureRecognizer(target: self, action: #selector(backSliderTapped(gestureRecognizer:)))
        backSlider.addGestureRecognizer(tapGestureRecognizerBack)
        
        // Set slider thumb image
        sitSlider.setThumbImage(#imageLiteral(resourceName: "thumb"), for: .normal)
        backSlider.setThumbImage(#imageLiteral(resourceName: "thumb"), for: .normal)
        
        
        // WKWebView setup
        let webConfiguration = WKWebViewConfiguration()
        let contentController = WKUserContentController()
        //message name for js bridge
        contentController.add(self, name: "boost")
        contentController.add(self, name: "boostEnd")
        contentController.add(self, name: "boostInterrupt")
        webConfiguration.userContentController = contentController
        // Init WKWebView
        tempWebView = WKWebView(frame: webContainer.bounds, configuration: webConfiguration)
        tempWebView.uiDelegate = self
        // Add to container
        webContainer.addSubview(tempWebView)
        
        // Constraints to fit container layout
        tempWebView.translatesAutoresizingMaskIntoConstraints = false
        tempWebView.leadingAnchor.constraint(equalTo: webContainer.leadingAnchor, constant: 0).isActive = true
        tempWebView.trailingAnchor.constraint(equalTo: webContainer.trailingAnchor, constant: 0).isActive = true
        tempWebView.topAnchor.constraint(equalTo: webContainer.topAnchor, constant: 0).isActive = true
        tempWebView.bottomAnchor.constraint(equalTo: webContainer.bottomAnchor, constant: 0).isActive = true
        
        // Load WKWebView resources
        let htmlPath = Bundle.main.path(forResource: "temperature", ofType: "html")
        let htmlUrl = URL(fileURLWithPath: htmlPath!, isDirectory: false)
        tempWebView.loadFileURL(htmlUrl, allowingReadAccessTo: htmlUrl)
        // Let WebView appear transparent and fit full content
        tempWebView.isOpaque = false
        tempWebView.backgroundColor = UIColor.clear
        tempWebView.scrollView.bounces = false
        tempWebView.scrollView.contentInsetAdjustmentBehavior = .never
        
        discBtn.backgroundColor = primaryColor
        discBtn.layer.cornerRadius = 10
        
        // Add Observer for listing to sensor state
        //nc.addObserver(self, selector: #selector(sensorStateChanged(_:)), name: Notification.Name("SensorEvent"), object: nil)
        nc.addObserver(self, selector: #selector(lostConnection(_:)), name: Notification.Name("BLEDisconnected"), object: nil)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // JS Bridge Observer for messages -> name is identifier not message
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        if (message.name == "boost") {
            sitSlider.isEnabled = false
            backSlider.isEnabled = false
            sitSlider.value = 100
            backSlider.value = 100
            BLEDevice.shared.startBoost()
        }
        if (message.name == "boostEnd") {
            // Back to last user values after boost ended
            if let storedSitValue: Int = preferences.object(forKey: "sitValue") as? Int{
                
                sitSlider.isEnabled = true
                backSlider.isEnabled = true
                
                BLEDevice.shared.writeHeatingSitValue(value: storedSitValue/2)
                sitSlider.value = Float(storedSitValue)
                
                //animate css effect for temperature
                switch storedSitValue {
                case 0 :
                    tempWebView.evaluateJavaScript("sitStageZero();")
                    break
                case 1..<26 :
                    tempWebView.evaluateJavaScript("sitStageOne();")
                    break
                case 26..<51 :
                    tempWebView.evaluateJavaScript("sitStageTwo();")
                    break
                case 51..<76 :
                    tempWebView.evaluateJavaScript("sitStageThree();")
                    break
                case 76..<101 :
                    tempWebView.evaluateJavaScript("sitStageFour();")
                    break
                default :
                    break
                }
            } else {
                sitSlider.isEnabled = true
                backSlider.isEnabled = true
                sitSlider.value = 0
                BLEDevice.shared.writeHeatingSitValue(value: 0)
                tempWebView.evaluateJavaScript("sitStageZero();")
            }
            // Back to last user values after boost ended
            if let storedBackValue: Int = preferences.object(forKey: "backValue") as? Int{
                BLEDevice.shared.writeHeatingBackValue(value: storedBackValue/2)
                backSlider.value = Float(storedBackValue)
                
                //animate css effect for temperature
                switch storedBackValue {
                case 0 :
                    tempWebView.evaluateJavaScript("backStageZero();")
                    break
                case 1..<26 :
                    tempWebView.evaluateJavaScript("backStageOne();")
                    break
                case 26..<51 :
                    tempWebView.evaluateJavaScript("backStageTwo();")
                    break
                case 51..<76 :
                    tempWebView.evaluateJavaScript("backStageThree();")
                    break
                case 76..<101 :
                    tempWebView.evaluateJavaScript("backStageFour();")
                    break
                default :
                    break
                }
            } else {
                backSlider.value = 0
                BLEDevice.shared.writeHeatingBackValue(value: 0)
                tempWebView.evaluateJavaScript("backStageZero();")
            }
        }
        if (message.name == "boostInterrupt") {
            sitSlider.value = 0
            backSlider.value = 0
            BLEDevice.shared.endBoost()
        }
    }
    
    
    @IBAction func switchStateChanged(_ sender: Any) {
        if (switchState.isOn) {
            sitSlider.isEnabled = true
            backSlider.isEnabled = true
            sitLabel.textColor = primaryColor
            backLabel.textColor = primaryColor
            // Enable webview button -> boost
            tempWebView.evaluateJavaScript("enableButton();")

            // Load last values from user setup
            if let storedSitValue: Int = preferences.object(forKey: "sitValue") as? Int{
                BLEDevice.shared.writeHeatingSitValue(value: storedSitValue/2)
                sitSlider.value = Float(storedSitValue)

                //animate css effect for temperature
                switch storedSitValue {
                case 0 :
                    tempWebView.evaluateJavaScript("sitStageZero();")
                    break
                case 1..<26 :
                    tempWebView.evaluateJavaScript("sitStageOne();")
                    break
                case 26..<51 :
                    tempWebView.evaluateJavaScript("sitStageTwo();")
                    break
                case 51..<76 :
                    tempWebView.evaluateJavaScript("sitStageThree();")
                    break
                case 76..<101 :
                    tempWebView.evaluateJavaScript("sitStageFour();")
                    break
                default :
                    break
                }
            }
            
            // Load last values from user setup
            if let storedBackValue: Int = preferences.object(forKey: "backValue") as? Int{
                BLEDevice.shared.writeHeatingBackValue(value: storedBackValue/2)
                backSlider.value = Float(storedBackValue)
                
                //animate css effect for temperature
                switch storedBackValue {
                case 0 :
                    tempWebView.evaluateJavaScript("backStageZero();")
                    break
                case 1..<26 :
                    tempWebView.evaluateJavaScript("backStageOne();")
                    break
                case 26..<51 :
                    tempWebView.evaluateJavaScript("backStageTwo();")
                    break
                case 51..<76 :
                    tempWebView.evaluateJavaScript("backStageThree();")
                    break
                case 76..<101 :
                    tempWebView.evaluateJavaScript("backStageFour();")
                    break
                default :
                    break
                }
            }
            
        } else {
            sitSlider.isEnabled = false
            backSlider.isEnabled = false
            sitLabel.textColor = UIColor.white
            backLabel.textColor = UIColor.white
            // Disable webview button -> boost
            tempWebView.evaluateJavaScript("disableButton();")
            // Interrupt boost while state -> OFF
            tempWebView.evaluateJavaScript("interruptBoost();")
            // Reset Temperature values on BLE board (Boost included -> delegated from boostEnd event via JS)
            BLEDevice.shared.disableTemperatureTab()
        }
    }
    
    @IBAction func sitValueChanged(_ sender: Any) {
        let intValueSit:Int = Int(sitSlider.value)
        print ("Sitzen ist auf \(intValueSit)")
        BLEDevice.shared.writeHeatingSitValue(value: (intValueSit/2))
        preferences.set(intValueSit, forKey: "sitValue")
        
        //animate css effect for temperature
        switch intValueSit {
        case 0 :
            tempWebView.evaluateJavaScript("sitStageZero();")
            break
        case 1..<26 :
            tempWebView.evaluateJavaScript("sitStageOne();")
            break
        case 26..<51 :
            tempWebView.evaluateJavaScript("sitStageTwo();")
            break
        case 51..<76 :
            tempWebView.evaluateJavaScript("sitStageThree();")
            break
        case 76..<101 :
            tempWebView.evaluateJavaScript("sitStageFour();")
            break
        default :
            break
        }
    }
    
    @objc func sitSliderTapped(gestureRecognizer: UIGestureRecognizer) {
        let pointTapped: CGPoint = gestureRecognizer.location(in: self.view)
        let positionOfSlider: CGPoint = sitSlider.frame.origin
        let widthOfSlider: CGFloat = sitSlider.frame.size.width
        let newValue = ((pointTapped.x - positionOfSlider.x) * CGFloat(sitSlider.maximumValue) / widthOfSlider)
        sitSlider.setValue(Float(newValue), animated: true)
        
        // Same arg as in value changed event -> BLE value
        let intValueSit:NSInteger = NSInteger(sitSlider.value)
        print ("Sitzen ist auf \(intValueSit)")
        BLEDevice.shared.writeHeatingSitValue(value: intValueSit/2)
        preferences.set(intValueSit, forKey: "sitValue")
        
        //animate css effect for temperature
        switch intValueSit {
        case 0 :
            tempWebView.evaluateJavaScript("sitStageZero();")
            break
        case 1..<26 :
            tempWebView.evaluateJavaScript("sitStageOne();")
            break
        case 26..<51 :
            tempWebView.evaluateJavaScript("sitStageTwo();")
            break
        case 51..<76 :
            tempWebView.evaluateJavaScript("sitStageThree();")
            break
        case 76..<101 :
            tempWebView.evaluateJavaScript("sitStageFour();")
            break
        default :
            break
        }
        
    }

    @IBAction func backValueChanged(_ sender: Any) {
        let intValueBack:Int = Int(backSlider.value)
        print ("Rücken ist auf \(intValueBack)")
        BLEDevice.shared.writeHeatingBackValue(value: intValueBack/2)
        preferences.set(intValueBack, forKey: "backValue")
        
        //animate css effect for temperature
        switch intValueBack {
        case 0 :
            tempWebView.evaluateJavaScript("backStageZero();")
            break
        case 1..<26 :
            tempWebView.evaluateJavaScript("backStageOne();")
            break
        case 26..<51 :
            tempWebView.evaluateJavaScript("backStageTwo();")
            break
        case 51..<76 :
            tempWebView.evaluateJavaScript("backStageThree();")
            break
        case 76..<101 :
            tempWebView.evaluateJavaScript("backStageFour();")
            break
        default :
            break
        }
    }
    
    @objc func backSliderTapped(gestureRecognizer: UIGestureRecognizer) {
        let pointTapped: CGPoint = gestureRecognizer.location(in: self.view)
        let positionOfSlider: CGPoint = backSlider.frame.origin
        let widthOfSlider: CGFloat = backSlider.frame.size.width
        let newValue = ((pointTapped.x - positionOfSlider.x) * CGFloat(backSlider.maximumValue) / widthOfSlider)
        backSlider.setValue(Float(newValue), animated: true)
        
        // Same arg as in value changed event -> BLE value
        let intValueBack:NSInteger = NSInteger(backSlider.value)
        print ("Rücken ist auf \(intValueBack)")
        BLEDevice.shared.writeHeatingBackValue(value: intValueBack/2)
        preferences.set(intValueBack, forKey: "backValue")
        
        //animate css effect for temperature
        switch intValueBack {
        case 0 :
            tempWebView.evaluateJavaScript("backStageZero();")
            break
        case 1..<26 :
            tempWebView.evaluateJavaScript("backStageOne();")
            break
        case 26..<51 :
            tempWebView.evaluateJavaScript("backStageTwo();")
            break
        case 51..<76 :
            tempWebView.evaluateJavaScript("backStageThree();")
            break
        case 76..<101 :
            tempWebView.evaluateJavaScript("backStageFour();")
            break
        default :
            break
        }
    }
    
    // Notification function called when device gets disconnected
    @objc func lostConnection(_ notificaiton:Notification) {
        disconnected()
    }
    
    func disconnected() {        
        //blur background
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.regular)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.view.addSubview(blurEffectView)
        
        popUpView2.center = self.view.center
        popUpView2.layer.cornerRadius = 10
        self.view.addSubview(popUpView2)
        
        // Add a gesture recognizer to the slider
        let tapGestureRecognizerInfo = UITapGestureRecognizer(target: self, action: #selector(infoTapped(gestureRecognizer:)))
        
        let infoView = UIImageView()
        infoView.image = UIImage(named: "questionMark")
        infoView.frame = CGRect(x: self.view.frame.width - 50, y: 30, width: 30, height: 45)
        infoView.addGestureRecognizer(tapGestureRecognizerInfo)
        infoView.isUserInteractionEnabled = true
        self.view.addSubview(infoView)
        
        tabBarController?.tabBar.isHidden = true
    }
    
    @objc func infoTapped(gestureRecognizer: UIGestureRecognizer) {
        //get current language
        let language = Locale.current.languageCode
        
        switch (language) {
        case "de":
            guard let url = URL(string: "https://www.kloeber.com/de/de/wooom/") else { return }
            UIApplication.shared.open(url)
            break
        case "en":
            guard let url = URL(string: "https://www.kloeber.com/kl/en/wooom/") else { return }
            UIApplication.shared.open(url)
            break
        case "fr":
            guard let url = URL(string: "https://www.kloeber.com/fr/fr/wooom/") else { return }
            UIApplication.shared.open(url)
            break
        case "nl":
            guard let url = URL(string: "https://www.kloeber.com/nl/nl/wooom/") else { return }
            UIApplication.shared.open(url)
            break
        default:
            guard let url = URL(string: "https://www.kloeber.com/kl/en/wooom/") else { return }
            UIApplication.shared.open(url)
            break
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

