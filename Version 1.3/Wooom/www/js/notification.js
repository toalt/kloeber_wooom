window.onload = function(){
    showIconBLE();
    showIconChair();
    showIconNFC();
};

function scanNFCTag() {
    window.webkit.messageHandlers.nfcScan.postMessage("NFCScan");
}

function activityIndicator() {
    document.body.style.backgroundImage = "url('background.jpg')";
    document.body.style.backgroundSize = "cover";
    var content = document.getElementById("slider");
    content.style.filter = "blur(20px)";
    
    var navi = document.getElementById("navi");
    navi.style.filter = "blur(20px)";
    
    var spin = document.getElementById("spinningWheel");
    spin.className = "isShown";
}

function connSuccess() {
    /*stop spinning wheel */
    var spin = document.getElementById("spinningWheel");
    spin.className = "isHidden";
    /*clear content*/
    var content = document.getElementById("slider");
    content.className = "hideContent";
    var naviBar = document.getElementById("navi");
    naviBar.className = "hideContent";
    
    var success = document.getElementById("connSuccess");
    success.className = 'show';
    
    /*alert native swift code*/
    window.webkit.messageHandlers.connection.postMessage("Ready");
}

function showIconBLE() {
    var icon = document.getElementById("iconBLE");
    var height = icon.clientHeight;
    icon.innerHTML = '<img src="notification_bluetooth.svg" />';
    icon.style.width = height+"px";
    icon.style.borderRadius = "50%";
}

function showIconChair() {
    var icon = document.getElementById("iconChair");
    var height = icon.clientHeight;
    icon.innerHTML = '<img src="notification_sitting.svg" />';
    icon.style.width = height+"px";
    icon.style.borderRadius = "50%";
}

function showIconNFC() {
    var icon = document.getElementById("iconNFC");
    var height = icon.clientHeight;
    icon.innerHTML = '<img src="notification_nfc.svg" />';
    icon.style.width = height+"px";
    icon.style.borderRadius = "50%";
}

function loadURLDE() {
    window.webkit.messageHandlers.loadURLDE.postMessage("loadURLDE");
}

function loadURLEN() {
    window.webkit.messageHandlers.loadURLEN.postMessage("loadURLEN");
}

function loadURLFR() {
    window.webkit.messageHandlers.loadURLFR.postMessage("loadURLFR");
}

function loadURLNL() {
    window.webkit.messageHandlers.loadURLNL.postMessage("loadURLNL");
}
