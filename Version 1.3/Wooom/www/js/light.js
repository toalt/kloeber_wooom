//check button state
var chair = document.getElementById("chair");
var light = document.getElementById("lightCone");

function enableLight() {
    chair.style.opacity = 0.9;
}

function disableLight() {
    chair.style.opacity = 0.3;
    light.style.opacity = 0.0;
}

function setLightValue(value) {
    light.style.opacity = value;
}

