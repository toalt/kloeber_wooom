//
//  FirstViewController.swift
//  Wooom
//
//  Created by Tobias Alt on 07.08.18.
//  Copyright © 2018 Kloeber. All rights reserved.
//

/////////////////////////////////////////////////////////////////////////////////////////
// History: V 0.1: Project start and testing for upload to iTunes Connect              //
//          V 0.2: Prototype version for Kloeber OrgaTec                               //
//                 - BlE functions enabled -> read/write                               //
//                 - No animations, no warnings, no custom user prefrences             //
//          V 0.3: Retrospective changes for Kloeber OrgaTec                           //
//                 - Detect when user left the chair                                   //
//                 - Slider touch handling                                             //
//                 - BLE Conncetion State Handling with PopUp Exceptions and Timer     //
//                 - Seagues now -> Unwind                                             //
//                 - Bluetooth state detection -> use case off -> reinit CBmanager     //
//          V 0.4: - Temperatur values halved (/2) -> boost not halved                 //
//                 - read chars -1 -> 1-60min // 1-250h // 1-101 voltage               //
//                 - paired devices removed -> alway launch notification screen        //
//                 - product variante check -> in notification VC added                //
//                 - App icon replaced with logo                                       //
//          V 1.0: - Manifest -> General -> NFC/BLE Frameworks linked                  //
//                 - Version based on version 0.4 (same setup)                         //
//                 - Sporadic disconnects from BLE board -> fix idea: detect wheter    //
//                   disconnected from Sensor -> Event or disconnect timeout from board //
//                   if disconnect sporadic -> reconnect paired device and do not show //
//                   reconnect dialog -> callback function in BLEDevice class ->       //
//                   didDisconnectPeripheral() // variable triggerDiscEvent            //
//                 - Core NFC Framework -> General -> Status -> Optional               //
/////////////////////////////////////////////////////////////////////////////////////////


import UIKit
import WebKit

class LightViewController: UIViewController,WKUIDelegate {

    // WKWebView for chair SVG
    var lightWebView: WKWebView!
    
    // IBOutlets
    @IBOutlet weak var stateSwitch: UISwitch!
    @IBOutlet weak var webContainer: UIView!
    @IBOutlet weak var dimSlider: UISlider!
    @IBOutlet weak var dimLabel: UILabel!
    @IBOutlet weak var tempSlider: UISlider!
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var userSettingsView: UIView!
    @IBOutlet weak var dialogButton: UIButton!
    @IBOutlet weak var popUpView2: UIView!
    @IBOutlet weak var reconBtn: UIButton!
    
    //corparate color
    let primaryColor = UIColor(rgb: 0xFFBB02)
    
    // Notication Center for update device state
    let nc = NotificationCenter.default
    
    // Create a global instance of NSUserDefaults class
    let preferences = UserDefaults.standard
    // Dialog PopUp
    var dialogValuesView:UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Color the switch background
        stateSwitch.backgroundColor = UIColor.gray
        stateSwitch.layer.cornerRadius = 16.0
        
        // Add a gesture recognizer to the slider
        let tapGestureRecognizerDim = UITapGestureRecognizer(target: self, action: #selector(dimSliderTapped(gestureRecognizer:)))
        dimSlider.addGestureRecognizer(tapGestureRecognizerDim)
        let tapGestureRecognizerTemp = UITapGestureRecognizer(target: self, action: #selector(tempSliderTapped(gestureRecognizer:)))
        tempSlider.addGestureRecognizer(tapGestureRecognizerTemp)
        
        // Set slider thumb image
        dimSlider.setThumbImage(#imageLiteral(resourceName: "thumb"), for: .normal)
        tempSlider.setThumbImage(#imageLiteral(resourceName: "thumb"), for: .normal)
        
        setDimSlider(slider: dimSlider)
        setTempSlider(slider: tempSlider)
        
        // WKWebView setup
        let webConfiguration = WKWebViewConfiguration()
        // Init WKWebView
        lightWebView = WKWebView(frame: webContainer.bounds, configuration: webConfiguration)
        lightWebView.uiDelegate = self
        // Add to container
        webContainer.addSubview(lightWebView)
        
        // Constraints to fit container layout
        lightWebView.translatesAutoresizingMaskIntoConstraints = false
        lightWebView.leadingAnchor.constraint(equalTo: webContainer.leadingAnchor, constant: 0).isActive = true
        lightWebView.trailingAnchor.constraint(equalTo: webContainer.trailingAnchor, constant: 0).isActive = true
        lightWebView.topAnchor.constraint(equalTo: webContainer.topAnchor, constant: 0).isActive = true
        lightWebView.bottomAnchor.constraint(equalTo: webContainer.bottomAnchor, constant: 0).isActive = true
        
        // Load WKWebView resources
        let htmlPath = Bundle.main.path(forResource: "light", ofType: "html")
        let htmlUrl = URL(fileURLWithPath: htmlPath!, isDirectory: false)
        lightWebView.loadFileURL(htmlUrl, allowingReadAccessTo: htmlUrl)
        // Let WebView appear transparent and fit full content
        lightWebView.isOpaque = false
        lightWebView.backgroundColor = UIColor.clear
        lightWebView.scrollView.bounces = false
        lightWebView.scrollView.contentInsetAdjustmentBehavior = .never
        
        // Style for blur effect when disconnected
        dialogButton.backgroundColor = primaryColor
        dialogButton.layer.cornerRadius = 10
        
        reconBtn.backgroundColor = primaryColor
        reconBtn.layer.cornerRadius = 10
        
        // Add Observer for listing to battery voltage characteristc
        //nc.addObserver(self, selector: #selector(sensorStateChanged(_:)), name: Notification.Name("SensorEvent"), object: nil)
        nc.addObserver(self, selector: #selector(lostConnection(_:)), name: Notification.Name("BLEDisconnected"), object: nil)
        
        // When first time launched - the user settings dialog is shown
        if (preferences.object(forKey: "firstTime") == nil) {
            preferences.set("1", forKey: "firstTime")
            DispatchQueue.main.asyncAfter(deadline: .now()+0.2) { //desired number of seconds
                self.showUserSettingsDialog()
            }
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func stateSwitchChanged(_ sender: Any) {
        if (stateSwitch.isOn) {
            print("--JETZT--")
            dimSlider.isEnabled = true
            tempSlider.isEnabled = true
            dimLabel.textColor = primaryColor
            tempLabel.textColor = primaryColor
            lightWebView.evaluateJavaScript("enableLight();")

            // Load last values from user setup
            if let storedDimValue: Int = preferences.object(forKey: "dimValue") as? Int{
                print ("Dim: ",storedDimValue)
                BLEDevice.shared.writeLedBrightValue(value: storedDimValue)
                dimSlider.value = Float(storedDimValue)
                switch storedDimValue {
                    case 0 :
                        lightWebView.evaluateJavaScript("setLightValue('0.0');")
                        break
                    case 1..<10 :
                        lightWebView.evaluateJavaScript("setLightValue('0.2');")
                        break
                    case 10..<21 :
                        lightWebView.evaluateJavaScript("setLightValue('0.3');")
                        break
                    case 21..<31 :
                        lightWebView.evaluateJavaScript("setLightValue('0.4');")
                        break
                    case 31..<41 :
                        lightWebView.evaluateJavaScript("setLightValue('0.5');")
                        break
                    case 41..<51 :
                        lightWebView.evaluateJavaScript("setLightValue('0.6');")
                        break
                    case 51..<61 :
                        lightWebView.evaluateJavaScript("setLightValue('0.7');")
                        break
                    case 61..<71 :
                        lightWebView.evaluateJavaScript("setLightValue('0.75');")
                        break
                    case 71..<81 :
                        lightWebView.evaluateJavaScript("setLightValue('0.8');")
                        break
                    case 81..<91 :
                        lightWebView.evaluateJavaScript("setLightValue('0.85');")
                        break
                    case 91..<101 :
                        lightWebView.evaluateJavaScript("setLightValue('0.9');")
                        break
                    default :
                        break
                }
            }
            
            if let storedTempValue: Int = preferences.object(forKey: "tempValue") as? Int{
                print ("Stored Temp value: ",storedTempValue)
                BLEDevice.shared.writeLedColorValue(value: storedTempValue)
                tempSlider.value = Float(storedTempValue)
            } else {
                tempSlider.value = 50
                BLEDevice.shared.writeLedColorValue(value: 50)
            }
            
        } else {
            dimSlider.isEnabled = false
            tempSlider.isEnabled = false
            dimLabel.textColor = UIColor.white
            tempLabel.textColor = UIColor.white
            lightWebView.evaluateJavaScript("disableLight();")
            // Reset LED values on BLE board
            BLEDevice.shared.disableLightTab()
            // Reset GUI values to 0
            dimSlider.value = 0
            tempSlider.value = 0
        }
    }
    
    @IBAction func dimValueChanged(_ sender: Any) {
        let intValueDim:NSInteger = NSInteger(dimSlider.value)
        print ("Dimmer ist auf \(intValueDim)")
        BLEDevice.shared.writeLedBrightValue(value: intValueDim)
        preferences.set(intValueDim, forKey: "dimValue")
        //animate css effect
        switch intValueDim {
        case 0 :
            lightWebView.evaluateJavaScript("setLightValue('0.0');")
            break
        case 1..<10 :
            lightWebView.evaluateJavaScript("setLightValue('0.2');")
            break
        case 10..<21 :
            lightWebView.evaluateJavaScript("setLightValue('0.3');")
            break
        case 21..<31 :
            lightWebView.evaluateJavaScript("setLightValue('0.4');")
            break
        case 31..<41 :
            lightWebView.evaluateJavaScript("setLightValue('0.5');")
            break
        case 41..<51 :
            lightWebView.evaluateJavaScript("setLightValue('0.6');")
            break
        case 51..<61 :
            lightWebView.evaluateJavaScript("setLightValue('0.7');")
            break
        case 61..<71 :
            lightWebView.evaluateJavaScript("setLightValue('0.75');")
            break
        case 71..<81 :
            lightWebView.evaluateJavaScript("setLightValue('0.8');")
            break
        case 81..<91 :
            lightWebView.evaluateJavaScript("setLightValue('0.85');")
            break
        case 91..<101 :
            lightWebView.evaluateJavaScript("setLightValue('0.9');")
            break
        default :
            break
        }
    }
    
    @objc func dimSliderTapped(gestureRecognizer: UIGestureRecognizer) {
        let pointTapped: CGPoint = gestureRecognizer.location(in: self.view)
        let positionOfSlider: CGPoint = dimSlider.frame.origin
        let widthOfSlider: CGFloat = dimSlider.frame.size.width
        let newValue = ((pointTapped.x - positionOfSlider.x) * CGFloat(dimSlider.maximumValue) / widthOfSlider)
        dimSlider.setValue(Float(newValue), animated: true)
        
        // Same arg as in value changed event -> BLE value
        let intValueDim:NSInteger = NSInteger(dimSlider.value)
        print ("Dimmer ist auf \(intValueDim)")
        BLEDevice.shared.writeLedBrightValue(value: intValueDim)
        preferences.set(intValueDim, forKey: "dimValue")
        switch intValueDim {
        case 0 :
            lightWebView.evaluateJavaScript("setLightValue('0.0');")
            break
        case 1..<10 :
            lightWebView.evaluateJavaScript("setLightValue('0.2');")
            break
        case 10..<21 :
            lightWebView.evaluateJavaScript("setLightValue('0.3');")
            break
        case 21..<31 :
            lightWebView.evaluateJavaScript("setLightValue('0.4');")
            break
        case 41..<51 :
            lightWebView.evaluateJavaScript("setLightValue('0.5');")
            break
        case 51..<61 :
            lightWebView.evaluateJavaScript("setLightValue('0.6');")
            break
        case 61..<71 :
            lightWebView.evaluateJavaScript("setLightValue('0.7');")
            break
        case 71..<81 :
            lightWebView.evaluateJavaScript("setLightValue('0.75');")
            break
        case 81..<91 :
            lightWebView.evaluateJavaScript("setLightValue('0.8');")
            break
        case 91..<101 :
            lightWebView.evaluateJavaScript("setLightValue('0.9');")
            break
        default :
            break
        }
    }
    
    @IBAction func tempValueChanged(_ sender: Any) {
        let intValueTemp:Int = Int(tempSlider.value)
        print ("Temperatur ist auf \(intValueTemp)")
        BLEDevice.shared.writeLedColorValue(value: intValueTemp)
        preferences.set(intValueTemp, forKey: "tempValue")
    }

    @objc func tempSliderTapped(gestureRecognizer: UIGestureRecognizer) {
        let pointTapped: CGPoint = gestureRecognizer.location(in: self.view)
        let positionOfSlider: CGPoint = tempSlider.frame.origin
        let widthOfSlider: CGFloat = tempSlider.frame.size.width
        let newValue = ((pointTapped.x - positionOfSlider.x) * CGFloat(tempSlider.maximumValue) / widthOfSlider)
        tempSlider.setValue(Float(newValue), animated: true)
        
        // Same arg as in value changed event -> BLE value
        let intValueTemp:NSInteger = NSInteger(tempSlider.value)
        print ("Temperatur ist auf \(intValueTemp)")
        BLEDevice.shared.writeLedColorValue(value: intValueTemp)
        preferences.set(intValueTemp, forKey: "tempValue")
    }
    
    func setDimSlider(slider:UISlider) {
        let tgl = CAGradientLayer()
        let frame = CGRect.init(x:0, y:0, width:slider.frame.size.width, height:5)
        tgl.frame = frame
        tgl.colors = [UIColor.gray.cgColor, primaryColor.cgColor]
        tgl.startPoint = CGPoint.init(x:0.0, y:0.5)
        tgl.endPoint = CGPoint.init(x:1.0, y:0.5)
        
        UIGraphicsBeginImageContextWithOptions(tgl.frame.size, tgl.isOpaque, 0.0);
        tgl.render(in: UIGraphicsGetCurrentContext()!)
        if let image = UIGraphicsGetImageFromCurrentImageContext() {
            UIGraphicsEndImageContext()
            
            image.resizableImage(withCapInsets: UIEdgeInsets.zero)
            
            slider.setMinimumTrackImage(image, for: .normal)
        }
    }
    
    func setTempSlider(slider:UISlider) {
        let tgl = CAGradientLayer()
        let frame = CGRect.init(x:0, y:0, width:slider.frame.size.width, height:5)
        tgl.frame = frame
        tgl.colors = [UIColor.blue.cgColor, UIColor.green.cgColor, primaryColor.cgColor]
        tgl.startPoint = CGPoint.init(x:0.0, y:0.5)
        tgl.endPoint = CGPoint.init(x:1.0, y:0.5)
        
        UIGraphicsBeginImageContextWithOptions(tgl.frame.size, tgl.isOpaque, 0.0);
        tgl.render(in: UIGraphicsGetCurrentContext()!)
        if let image = UIGraphicsGetImageFromCurrentImageContext() {
            UIGraphicsEndImageContext()
            
            image.resizableImage(withCapInsets: UIEdgeInsets.zero)
            
            slider.setMinimumTrackImage(image, for: .normal)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Notification function called when device gets disconnected
    @objc func lostConnection(_ notificaiton:Notification) {
        disconnected()
    }
    
    // Notification function called when sensor state changed -> user not sitting?
    /*@objc func sensorStateChanged(_ notificaiton:Notification) {
        //check sensor state
        if (BLEDevice.shared.sensorState == 0) {
            disconnectedFromSensor()
        }
    }*/
    
    func showUserSettingsDialog() {
        //blur background
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.regular)
        dialogValuesView = UIVisualEffectView(effect: blurEffect)
        dialogValuesView.frame = view.bounds
        dialogValuesView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.view.addSubview(dialogValuesView)
        
        userSettingsView.center = self.view.center
        userSettingsView.layer.cornerRadius = 10
        self.view.addSubview(userSettingsView)
        
        // force the user to push the btn
        tabBarController?.tabBar.isHidden = true
    }
    
    @IBAction func hideSettingsDialog(_ sender: Any) {
        dialogValuesView.removeFromSuperview()
        userSettingsView.removeFromSuperview()
        // force the user to push the btn
        tabBarController?.tabBar.isHidden = false
    }
    
    func disconnected() {        
        //blur background
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.regular)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.view.addSubview(blurEffectView)
        
        popUpView2.center = self.view.center
        popUpView2.layer.cornerRadius = 10
        self.view.addSubview(popUpView2)
        
        // Add a gesture recognizer to the slider
        let tapGestureRecognizerInfo = UITapGestureRecognizer(target: self, action: #selector(infoTapped(gestureRecognizer:)))
        
        let infoView = UIImageView()
        infoView.image = UIImage(named: "questionMark")
        infoView.frame = CGRect(x: self.view.frame.width - 50, y: 30, width: 30, height: 45)
        infoView.addGestureRecognizer(tapGestureRecognizerInfo)
        infoView.isUserInteractionEnabled = true
        self.view.addSubview(infoView)
        
        //force the user to push the btn
        tabBarController?.tabBar.isHidden = true
    }
    
    @objc func infoTapped(gestureRecognizer: UIGestureRecognizer) {
        //get current language
        let language = Locale.current.languageCode
        
        switch (language) {
        case "de":
            guard let url = URL(string: "https://www.kloeber.com/de/de/wooom/") else { return }
            UIApplication.shared.open(url)
            break
        case "en":
            guard let url = URL(string: "https://www.kloeber.com/kl/en/wooom/") else { return }
            UIApplication.shared.open(url)
            break
        case "fr":
            guard let url = URL(string: "https://www.kloeber.com/fr/fr/wooom/") else { return }
            UIApplication.shared.open(url)
            break
        case "nl":
            guard let url = URL(string: "https://www.kloeber.com/nl/nl/wooom/") else { return }
            UIApplication.shared.open(url)
            break
        default:
            guard let url = URL(string: "https://www.kloeber.com/kl/en/wooom/") else { return }
            UIApplication.shared.open(url)
            break
        }
    }
    
}

//convert hex color #
extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(rgb: Int) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF
        )
    }
}
