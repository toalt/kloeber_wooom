//
//  Peripheral.swift
//  Wooom
//
//  Created by Tobias Alt on 03.09.18.
//  Copyright © 2018 Kloeber. All rights reserved.
//

import CoreBluetooth

class BLEDevice: NSObject,CBCentralManagerDelegate,CBPeripheralDelegate {
    
    // Singleton pattern
    static let shared = BLEDevice()
    
    // Create a global instance of NSUserDefaults class
    let preferences = UserDefaults.standard
    
    // Notify other view controller
    let nc = NotificationCenter.default
    
    // Global read values -> visible everywhere with singleton object
    var voltage: Int! 
    var remainHours: Int!
    var remainMin: Int!
    var isConnected = false
    var productType: Int!
    var pwrSupplyState: Int!
    
    // CBCentralManager
    var centralDevice: CBCentralManager!
    var blePeripheral: CBPeripheral!
    // Interval timer for scanning periperhals ->
    var timer = Timer()
    var offlineTimer = Timer()

    
    // Variable to decide wheter Sensor disc or common disconnect -> not both events should be triggered simultaniously
    var triggerDiscEvent: Int! = 0
    
    // Variables to store time remain update
    var hours : UInt8 = 1
    var min : UInt8 = 1
    
    // UUID of characteristics
    let LED_BRIGHT_UUID = CBUUID(string: "3c150807-b1ca-427f-9957-fe3863f99e0a")
    let LED_Color_UUID = CBUUID(string: "2cf99398-baa9-4685-b9ca-00fefcf85d38")
    
    let MASSAGE_MODE_UUID = CBUUID(string: "4cf57a40-8842-4e88-8f3b-7db7b331f02f")
    let MASSAGE_TOP_UUID = CBUUID(string: "19b83c07-b362-4ded-ae8d-77d52fd99b34")
    let MASSAGE_MID_UUID = CBUUID(string: "f6928b8d-f2a3-4f0b-82e2-2d0b955fe9e6")
    let MASSAGE_BOT_UUID = CBUUID(string: "442d6046-efc7-4a10-b6db-5e82c1334132")
    
    let HEATING_SIT_UUID = CBUUID(string: "c85a0adb-bccd-41ea-bbe5-e49f8350c81e")
    let HEATING_BACK_UUID = CBUUID(string: "da2f78c1-1e28-4cda-8757-349d4659ef2b")
    
    let BATTERY_TIME_REMAIN_UUID = CBUUID(string: "f7002312-190e-46a5-9f8a-a30af1502cf8")
    let BATTERY_VOLTAGE_UUID = CBUUID(string: "ceac84d2-9871-44ad-82df-57911db8e0aa")
    let PWR_SUPPLY_UUID = CBUUID(string: "83b779b1-c10a-4a6e-ba24-8322a6088138")
    
    let PRODUCT_TYPE_UUID = CBUUID(string: "b8cad0af-8c8f-47a6-b689-04e9ab08ba25")
    
    // Reference each characteristc
    var writeLedBrightCharacteristic:CBCharacteristic!
    var writeLedColorCharacteristic:CBCharacteristic!
    
    var writeMassageModeCharacteristic:CBCharacteristic!
    var writeMassageTopCharacteristic:CBCharacteristic!
    var writeMassageMidCharacteristic:CBCharacteristic!
    var writeMassageBotCharacteristic:CBCharacteristic!
    
    var writeHeatingSitCharacteristic:CBCharacteristic!
    var writeHeatingBackCharacteristic:CBCharacteristic!
    
    var readBatteryTimeRemainCharacteristic:CBCharacteristic!
    var readBatteryVoltageCharacteristic:CBCharacteristic!
    var readPwrSupplyCharacteristic:CBCharacteristic!
    
    var readProductTypeCharacteristic:CBCharacteristic!
    
    private override init() {
        // don't forget to make this private
    }
    
    //init CBCentral Manager - NOTE! this function will be called ONCE - then the CBCentralManager is globally allocated -> will be called when needed -> first at launchController
    func initCBManager () {
        // reset timer
        timer.invalidate()
        centralDevice = CBCentralManager(delegate: self, queue: nil)
        print("CBManager successfully instantiate")
        timer = Timer.scheduledTimer(timeInterval: 10.0, target: self,selector: #selector(onTimeUp(t:)), userInfo: nil, repeats: true)
    }
    
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        if central.state == CBManagerState.poweredOn {
            central.scanForPeripherals(withServices: nil, options: nil)
            print("Start Scanning...")
        } else {
            // When Bluetooth is OFF on the iPhone device - force to show up the system dialog recursive after 10 sec
            timer.invalidate() //disable custom dialog
            offlineTimer = Timer.scheduledTimer(timeInterval: 10.0, target: self,selector: #selector(bleOffline(t:)), userInfo: nil, repeats: true)
            print("Bluetooth not available.")
        }
    }
    
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        
        // local advertisment Name (if the board device changes)
        let localName = advertisementData[CBAdvertisementDataLocalNameKey]
        print("The local device name is \(String(describing: localName)).")
        let deviceName = localName as! String?
        
        // fetch the preferences
        let storedDeviceName = preferences.object(forKey: "pairedDevice") as! String
        
        // print("Verfügbare Geräte: \(peripheral)")
        if(deviceName == storedDeviceName) {
            blePeripheral = peripheral
            blePeripheral.delegate = self
            
            centralDevice.connect(blePeripheral, options: nil)
            centralDevice.stopScan()
            timer.invalidate()
        }
    }

    // Timer event
    @objc func onTimeUp(t: Timer) {
        //print ("---Break---")
        centralDevice.stopScan()
        nc.post(name: Notification.Name("Timeout"), object: nil)
        timer.invalidate()
    }
    
    // Timer event for offline state
    @objc func bleOffline(t: Timer) {
        initCBManager()
        offlineTimer.invalidate()
    }
    
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        print("Connected!")

        blePeripheral.discoverServices(nil)
        isConnected = true
        nc.post(name: Notification.Name("BLEConnected"), object: nil)
    }
    
    
    func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
        // Deny two events simultaniously -> SensorEvent & OfflineEvent
        if (triggerDiscEvent == 0) {
            nc.post(name: Notification.Name("BLEDisconnected"), object: nil)
        } else {
            // Reset disconnect conditions
            triggerDiscEvent = 0
        }
        print("Disconnected!")
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        guard let services = peripheral.services else { return }
        for service in services {
            print(service)
            blePeripheral.discoverCharacteristics(nil, for: service)
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        
        guard let characteristics = service.characteristics else { return }
        
        for characteristic in characteristics {
            
            // Allocate each characteristic
            switch(characteristic.uuid.uuidString) {
                case LED_BRIGHT_UUID.uuidString:
                    writeLedBrightCharacteristic = characteristic
                    print("--LED Bright gefunden!--")
                    break
                case LED_Color_UUID.uuidString:
                    writeLedColorCharacteristic = characteristic
                    print("--LED Color gefunden!--")
                break
                case MASSAGE_MODE_UUID.uuidString:
                    writeMassageModeCharacteristic = characteristic
                    print("--Massage Modus gefunden!--")
                    break
                case MASSAGE_TOP_UUID.uuidString:
                    writeMassageTopCharacteristic = characteristic
                    print("--Massage Top gefunden!--")
                    break
                case MASSAGE_MID_UUID.uuidString:
                    writeMassageMidCharacteristic = characteristic
                    print("--Massage Mid gefunden!--")
                    break
                case MASSAGE_BOT_UUID.uuidString:
                    writeMassageBotCharacteristic = characteristic
                    print("--Massage Bot gefunden!--")
                    break
                case HEATING_SIT_UUID.uuidString:
                    writeHeatingSitCharacteristic = characteristic
                    print("--Heating Sit gefunden!--")
                    break
                case HEATING_BACK_UUID.uuidString:
                    writeHeatingBackCharacteristic = characteristic
                    print("--Heating Back gefunden!--")
                    break
                case BATTERY_TIME_REMAIN_UUID.uuidString:
                     readBatteryTimeRemainCharacteristic = characteristic
                     print ("--Batterie Zeit gefunden!--")
                     blePeripheral.setNotifyValue(true, for: readBatteryTimeRemainCharacteristic) //Normen muss auf dem Board notify setzen
                     blePeripheral.readValue(for: readBatteryTimeRemainCharacteristic)
                     break
                case BATTERY_VOLTAGE_UUID.uuidString:
                     readBatteryVoltageCharacteristic = characteristic
                     print ("--Batterie Voltage gefunden!--")
                     blePeripheral.setNotifyValue(true, for: readBatteryVoltageCharacteristic) //Normen muss auf dem Board notify setzen
                     blePeripheral.readValue(for: readBatteryVoltageCharacteristic)
                     break
                case PRODUCT_TYPE_UUID.uuidString:
                     readProductTypeCharacteristic = characteristic
                     blePeripheral.readValue(for: readProductTypeCharacteristic)
                     print ("--Stuhl Variante gefunden!--")
                     break
                case PWR_SUPPLY_UUID.uuidString:
                     readPwrSupplyCharacteristic = characteristic
                     blePeripheral.setNotifyValue(true, for: readPwrSupplyCharacteristic)
                     blePeripheral.readValue(for: readPwrSupplyCharacteristic)
                     print ("--Power Supply gefunden!--")
            default: break
      
            }
        }
    }
    
    // values can be observed with proptert .notify on BLE Board
    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
            switch(characteristic.uuid.uuidString) {
            case BATTERY_TIME_REMAIN_UUID.uuidString:

                let value = [UInt8](readBatteryTimeRemainCharacteristic.value!)
                
                if (value.count > 0) {
                    hours = value[0]
                    min = value[1]
                }
                
                // -1 because the controller sends values from 1-250 h 1-60 m (no null values forced)
                remainHours = Int(hours) - 1
                remainMin = Int(min) - 1
                // Print for testing
                print ("Voltage hours: \(String(describing: remainHours))")
                print ("Voltage min: \(String(describing: remainMin))")
                nc.post(name: Notification.Name("TimeRemainUpdate"), object: nil)
                break
            case BATTERY_VOLTAGE_UUID.uuidString:
                let voltageRead = readBatteryVoltageCharacteristic.value![0]
                // -1 because the controller sends values from 1-101 (no null values forced)
                voltage = Int(voltageRead) - 1
                // Print for testing
                //voltage = 10
                print ("Voltage power: \(String(describing: voltage))%")
                nc.post(name: Notification.Name("VoltageUpdate"), object: nil)
                break
            case PRODUCT_TYPE_UUID.uuidString:
                let type = readProductTypeCharacteristic.value![0]
                productType = Int(type)
                // Print for testing
                print ("Product version type: \(String(describing: productType))")
                preferences.set(productType, forKey: "version")
                break
            case PWR_SUPPLY_UUID.uuidString:
                let supplyType = readPwrSupplyCharacteristic.value![0]
                pwrSupplyState = Int(supplyType)
                // Print for testing
                print ("Power Supply State: \(String(describing: pwrSupplyState))")

                // 1=Akku, 2=Netzteil+Akku läd, 3=Netzteil ohne Akku
                if (pwrSupplyState == 1) {
                    // Notify controller for supply mode -> change text/img
                    nc.post(name: Notification.Name("Pack"), object: nil)
                    print("--Notification update Akku normal--")
                }
                if (pwrSupplyState == 2) {
                    // Notify controller for supply mode -> change text/img
                    nc.post(name: Notification.Name("PackAndSpark"), object: nil)
                    print("--Notification update Strommodus--")
                }
                if (pwrSupplyState == 3) {
                    // Notify controller for supply mode -> change text/img
                    nc.post(name: Notification.Name("Spark"), object: nil)
                    print("--Notification update Kein Akku--")
                }
                break
            default:
                break
        }
    }
    
    //-----------------------------------------------//
    //------------CUSTOM FUNCTIONS------------------//
    //---------------------------------------------//
    
    
    // LED slider 1
    func writeLedBrightValue (value:NSInteger) {
        var parameter = value
        let myData = NSData(bytes: &parameter, length: 1)
        if (writeLedBrightCharacteristic != nil) {
            blePeripheral?.writeValue(myData as Data, for: writeLedBrightCharacteristic, type: .withoutResponse)
            print ("write LED Bright")
            print (parameter)
        }
    }
    
    // LED slider 2
    func writeLedColorValue (value:NSInteger) {
        var parameter = value
        let myData = NSData(bytes: &parameter, length: 1)
        if (writeLedColorCharacteristic != nil) {
            blePeripheral?.writeValue(myData as Data, for: writeLedColorCharacteristic, type: .withoutResponse)
            print ("write LED Color")
            print (parameter)
        }
    }
    
    // Massage modus
    func writeMassageModusValue (value:NSInteger) {
        var parameter = value
        let myData = NSData(bytes: &parameter, length: 1)
        if (writeMassageModeCharacteristic != nil) {
            blePeripheral?.writeValue(myData as Data, for: writeMassageModeCharacteristic, type: .withoutResponse)
            print ("write Massage Modus")
            print(parameter)
        }
    }
    
    // Massage slider 1
    func writeMassageTopValue (value:NSInteger) {
        var parameter = value
        let myData = NSData(bytes: &parameter, length: 1)
        if (writeMassageTopCharacteristic != nil) {
            blePeripheral?.writeValue(myData as Data, for: writeMassageTopCharacteristic, type: .withoutResponse)
            print ("write Massage Top")
        }
    }
    
    // Massage slider 2
    func writeMassageMidValue (value:NSInteger) {
        var parameter = value
        let myData = NSData(bytes: &parameter, length: 1)
        if (writeMassageMidCharacteristic != nil) {
            blePeripheral?.writeValue(myData as Data, for: writeMassageMidCharacteristic, type: .withoutResponse)
            print ("write Massage Mid")
        }
    }
    
    // Massage slider 3
    func writeMassageBotValue (value:NSInteger) {
        var parameter = value
        let myData = NSData(bytes: &parameter, length: 1)
        if (writeMassageBotCharacteristic != nil) {
            blePeripheral?.writeValue(myData as Data, for: writeMassageBotCharacteristic, type: .withoutResponse)
            print ("write Massage Bot")
        }
    }
    
    // Heating slider 1
    func writeHeatingSitValue (value:NSInteger) {
        var parameter = value
        let myData = NSData(bytes: &parameter, length: 1)
        if (writeHeatingSitCharacteristic != nil) {
            blePeripheral?.writeValue(myData as Data, for: writeHeatingSitCharacteristic, type: .withoutResponse)
            print ("write Heating Sit")
            print(parameter)
        }
    }
    
    // Heating slider 2
    func writeHeatingBackValue (value:NSInteger) {
        var parameter = value
        let myData = NSData(bytes: &parameter, length: 1)
        if (writeHeatingBackCharacteristic != nil) {
            blePeripheral?.writeValue(myData as Data, for: writeHeatingBackCharacteristic, type: .withoutResponse)
            print ("write Heating Back")
            print(parameter)
        }
    }
    
    // Start Boost
    func startBoost () {
        // Enable both heatings to 100
        var heating = 100
        let myData = NSData(bytes: &heating, length: 1)
        print("Starte Boost...")
        if (writeHeatingSitCharacteristic != nil) {
            blePeripheral?.writeValue(myData as Data, for: writeHeatingSitCharacteristic, type: .withoutResponse)
            print ("write Boost 1")
        }
        if (writeHeatingBackCharacteristic != nil) {
            blePeripheral?.writeValue(myData as Data, for: writeHeatingBackCharacteristic, type: .withoutResponse)
            print ("write Boost 2")
        }
        
    }
    
    // End Boost
    func endBoost () {
        // Set heating values back to 0
        var heating = 0
        let myData = NSData(bytes: &heating, length: 1)
        print("Boost Time Over")
        if (writeHeatingSitCharacteristic != nil) {
            blePeripheral?.writeValue(myData as Data, for: writeHeatingSitCharacteristic, type: .withoutResponse)
            print ("Reset Boost 1")
        }
        if (writeHeatingBackCharacteristic != nil) {
            blePeripheral?.writeValue(myData as Data, for: writeHeatingBackCharacteristic, type: .withoutResponse)
            print ("Reset Boost 2")
        }
    }
    
    // Disconnect active periperhal
    func disconnectBLEDevice() {
        resetAllValues()
        // deny alert from disconnect event -> delegate method 
        triggerDiscEvent = 1
        centralDevice.cancelPeripheralConnection(blePeripheral)
    }
    
    // Switch for disable light tab -> reset all values on 0 -> only on the physical board -> app shows last values greyed out
    func disableLightTab() {
        // Set LED values back to 0
        var light = 0
        let myData = NSData(bytes: &light, length: 1)
        if (writeLedBrightCharacteristic != nil) {
            blePeripheral?.writeValue(myData as Data, for: writeLedBrightCharacteristic, type: .withoutResponse)
            print ("Reset LED 1")
        }
        if (writeLedColorCharacteristic != nil) {
            blePeripheral?.writeValue(myData as Data, for: writeLedColorCharacteristic, type: .withoutResponse)
            print ("Reset LED 2")
        }
        print ("Tab Light BLE OFF")
    }
    
    // Switch for disable massage tab -> reset all values on 0 -> only on the physical board -> app shows last values greyed out
    func disableMassageTab() {
        // Set Massage motor values back to 0
        var motor = 0
        let myData = NSData(bytes: &motor, length: 1)
        if (writeMassageTopCharacteristic != nil) {
            blePeripheral?.writeValue(myData as Data, for: writeMassageTopCharacteristic, type: .withoutResponse)
            print ("Reset Massage 1")
        }
        if (writeMassageMidCharacteristic != nil) {
            blePeripheral?.writeValue(myData as Data, for: writeMassageMidCharacteristic, type: .withoutResponse)
            print ("Reset Massage 2")
        }
        if (writeMassageBotCharacteristic != nil) {
            blePeripheral?.writeValue(myData as Data, for: writeMassageBotCharacteristic, type: .withoutResponse)
            print ("Reset Massage 3")
        }
        print ("Tab Massage BLE OFF")
    }
    
    // Switch for disable temperature tab -> reset all values on 0 -> only on the physical board -> app shows last values greyed out
    func disableTemperatureTab() {
        // Set Temperature values back to 0
        var temp = 0
        let myData = NSData(bytes: &temp, length: 1)
        if (writeHeatingSitCharacteristic != nil) {
            blePeripheral?.writeValue(myData as Data, for: writeHeatingSitCharacteristic, type: .withoutResponse)
            print ("Reset Temp 1")
        }
        if (writeHeatingBackCharacteristic != nil) {
            blePeripheral?.writeValue(myData as Data, for: writeHeatingBackCharacteristic, type: .withoutResponse)
            print ("Reset Temp 2")
        }
        print ("Tab Temperature BLE OFF")
    }
    
    // Reset all write values on leaving
    func resetAllValues () {
        var reset = 0
        let myData = NSData(bytes: &reset, length: 1)
        if (writeLedBrightCharacteristic != nil) {
            blePeripheral?.writeValue(myData as Data, for: writeLedBrightCharacteristic, type: .withoutResponse)
            print ("Reset LED 1")
        }
        if (writeLedColorCharacteristic != nil) {
            blePeripheral?.writeValue(myData as Data, for: writeLedColorCharacteristic, type: .withoutResponse)
            print ("Reset LED 2")
        }
        if (writeMassageTopCharacteristic != nil) {
            blePeripheral?.writeValue(myData as Data, for: writeMassageTopCharacteristic, type: .withoutResponse)
            print ("Reset Massage 1")
        }
        if (writeMassageMidCharacteristic != nil) {
            blePeripheral?.writeValue(myData as Data, for: writeMassageMidCharacteristic, type: .withoutResponse)
            print ("Reset Massage 2")
        }
        if (writeMassageBotCharacteristic != nil) {
            blePeripheral?.writeValue(myData as Data, for: writeMassageBotCharacteristic, type: .withoutResponse)
            print ("Reset Massage 3")
        }
        if (writeHeatingSitCharacteristic != nil) {
            blePeripheral?.writeValue(myData as Data, for: writeHeatingSitCharacteristic, type: .withoutResponse)
            print ("Reset Heating 1")
        }
        if (writeHeatingBackCharacteristic != nil) {
            blePeripheral?.writeValue(myData as Data, for: writeHeatingBackCharacteristic, type: .withoutResponse)
            print ("Reset Heating 2")
        }
        print ("All write values resettet!")
    }
    
}
