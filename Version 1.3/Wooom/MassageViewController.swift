//
//  SecondViewController.swift
//  Wooom
//
//  Created by Tobias Alt on 07.08.18.
//  Copyright © 2018 Kloeber. All rights reserved.
//

import UIKit
import WebKit

class MassageViewController: UIViewController,WKUIDelegate,WKNavigationDelegate,WKScriptMessageHandler {

    // WKWebView for chair SVG
    var massageWebView: WKWebView!
    
    // IBOutlets
    @IBOutlet weak var switchState: UISwitch!
    @IBOutlet weak var webContainer: UIView!
    @IBOutlet weak var topSlider: UISlider!
    @IBOutlet weak var midSlider: UISlider!
    @IBOutlet weak var botSlider: UISlider!
    @IBOutlet weak var popUpView2: UIView!
    @IBOutlet weak var discBtn: UIButton!
    
    // Corparate color
    let primaryColor = UIColor(rgb: 0xFFBB02)
    
    // Last modus from user saved
    var lastModus:String = "one";
    
    // Notication Center for update device state
    let nc = NotificationCenter.default
    
    // Create a global instance of NSUserDefaults class
    let preferences = UserDefaults.standard
    
    var language: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Color the switch background
        switchState.backgroundColor = UIColor.gray
        switchState.layer.cornerRadius = 16.0
        
        // Add a gesture recognizer to the slider
        let tapGestureRecognizerTop = UITapGestureRecognizer(target: self, action: #selector(topSliderTapped(gestureRecognizer:)))
        topSlider.addGestureRecognizer(tapGestureRecognizerTop)
        let tapGestureRecognizerMid = UITapGestureRecognizer(target: self, action: #selector(midSliderTapped(gestureRecognizer:)))
        midSlider.addGestureRecognizer(tapGestureRecognizerMid)
        let tapGestureRecognizerBot = UITapGestureRecognizer(target: self, action: #selector(botSliderTapped(gestureRecognizer:)))
        botSlider.addGestureRecognizer(tapGestureRecognizerBot)
        
        // Image for slider thumb
        topSlider.setThumbImage(#imageLiteral(resourceName: "thumb"), for: .normal)
        midSlider.setThumbImage(#imageLiteral(resourceName: "thumb"), for: .normal)
        botSlider.setThumbImage(#imageLiteral(resourceName: "thumb"), for: .normal)
        
        // WKWebView setup
        let webConfiguration = WKWebViewConfiguration()
        let contentController = WKUserContentController()
        //message name for js bridge
        contentController.add(self, name: "modusOne")
        contentController.add(self, name: "modusTwo")
        contentController.add(self, name: "modusThree")
        contentController.add(self, name: "modusFour")
        webConfiguration.userContentController = contentController
        // Init WKWebView
        massageWebView = WKWebView(frame: webContainer.bounds, configuration: webConfiguration)
        massageWebView.uiDelegate = self
        massageWebView.navigationDelegate = self
        // Add to container
        webContainer.addSubview(massageWebView)
        
        // Constraints to fit container layout
        massageWebView.translatesAutoresizingMaskIntoConstraints = false
        massageWebView.leadingAnchor.constraint(equalTo: webContainer.leadingAnchor, constant: 0).isActive = true
        massageWebView.trailingAnchor.constraint(equalTo: webContainer.trailingAnchor, constant: 0).isActive = true
        massageWebView.topAnchor.constraint(equalTo: webContainer.topAnchor, constant: 0).isActive = true
        massageWebView.bottomAnchor.constraint(equalTo: webContainer.bottomAnchor, constant: 0).isActive = true
        
        var htmlPath: String?
        
        //get current language
        language = Locale.current.languageCode
        //print("LANG KEY",language)
        switch (language) {
        case "de":
            htmlPath = Bundle.main.path(forResource: "massageDE", ofType: "html")
            break
        case "en":
            htmlPath = Bundle.main.path(forResource: "massageEN", ofType: "html")
            break
        case "fr":
            htmlPath = Bundle.main.path(forResource: "massageFR", ofType: "html")
            break
        case "nl":
            htmlPath = Bundle.main.path(forResource: "massageNL", ofType: "html")
            break
        default:
            htmlPath = Bundle.main.path(forResource: "massageEN", ofType: "html")
            break
        }
        
        // Load WKWebView resources
        let htmlUrl = URL(fileURLWithPath: htmlPath!, isDirectory: false)
        massageWebView.loadFileURL(htmlUrl, allowingReadAccessTo: htmlUrl)
        // Let WebView appear transparent and fit full content
        massageWebView.isOpaque = false
        massageWebView.backgroundColor = UIColor.clear
        massageWebView.scrollView.bounces = false
        massageWebView.scrollView.contentInsetAdjustmentBehavior = .never
        
        // Add Observer for listing to sensor state
        //nc.addObserver(self, selector: #selector(sensorStateChanged(_:)), name: Notification.Name("SensorEvent"), object: nil)
        nc.addObserver(self, selector: #selector(lostConnection(_:)), name: Notification.Name("BLEDisconnected"), object: nil)
        
        discBtn.backgroundColor = primaryColor
        discBtn.layer.cornerRadius = 10
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        //load the last user state
        if let storedModus: String = preferences.object(forKey: "modus") as? String{
            massageWebView.evaluateJavaScript("setModus('\(storedModus)');")
        } else {
            massageWebView.evaluateJavaScript("setModus('\("one")');")
        }
    }
    
    // JS Bridge Observer for messages -> name is identifier not message
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        if (message.name == "modusOne") {
            BLEDevice.shared.writeMassageModusValue(value: 1)
            preferences.set("one", forKey: "modus")
            print("Modus",1)
        }
        if (message.name == "modusTwo") {
            BLEDevice.shared.writeMassageModusValue(value: 2)
            preferences.set("two", forKey: "modus")
            print("Modus",2)
        }
        if (message.name == "modusThree") {
            BLEDevice.shared.writeMassageModusValue(value: 3)
            preferences.set("three", forKey: "modus")
            print("Modus",3)
        }
        if (message.name == "modusFour") {
            BLEDevice.shared.writeMassageModusValue(value: 4)
            preferences.set("four", forKey: "modus")
            print("Modus",4)
        }
    }
    
    // Switch Slider to en-/disable whole tab
    @IBAction func currentState(_ sender: Any) {
        if switchState.isOn {
            topSlider.isEnabled = true
            midSlider.isEnabled = true
            botSlider.isEnabled = true
            massageWebView.evaluateJavaScript("enableModus();")

            // Load last values from user setup
            if let storedTopValue: Int = preferences.object(forKey: "topValue") as? Int{
                BLEDevice.shared.writeMassageTopValue(value: storedTopValue)
                topSlider.value = Float(storedTopValue)
                //animate css effect for massage
                switch storedTopValue {
                case 0 :
                    massageWebView.evaluateJavaScript("stageZeroTop();")
                    break
                case 1..<26 :
                    massageWebView.evaluateJavaScript("stageOneTop();")
                    break
                case 26..<51 :
                    massageWebView.evaluateJavaScript("stageTwoTop();")
                    break
                case 51..<76 :
                    massageWebView.evaluateJavaScript("stageThreeTop();")
                    break
                case 76..<101 :
                    massageWebView.evaluateJavaScript("stageFourTop();")
                    break
                default :
                    break
                }
            }
            // Load last values from user setup
            if let storedMidValue: Int = preferences.object(forKey: "midValue") as? Int{
                BLEDevice.shared.writeMassageMidValue(value: storedMidValue)
                midSlider.value = Float(storedMidValue)
                //animate css effect for massage
                switch storedMidValue {
                case 0 :
                    massageWebView.evaluateJavaScript("stageZeroMid();")
                    break
                case 1..<26 :
                    massageWebView.evaluateJavaScript("stageOneMid();")
                    break
                case 26..<51 :
                    massageWebView.evaluateJavaScript("stageTwoMid();")
                    break
                case 51..<76 :
                    massageWebView.evaluateJavaScript("stageThreeMid();")
                    break
                case 76..<101 :
                    massageWebView.evaluateJavaScript("stageFourMid();")
                    break
                default :
                    break
                }
            }
            // Load last values from user setup
            if let storedBotValue: Int = preferences.object(forKey: "botValue") as? Int{
                BLEDevice.shared.writeMassageBotValue(value: storedBotValue)
                botSlider.value = Float(storedBotValue)
                //animate css effect for massage
                switch storedBotValue {
                case 0 :
                    massageWebView.evaluateJavaScript("stageZeroBot();")
                    break
                case 1..<26 :
                    massageWebView.evaluateJavaScript("stageOneBot();")
                    break
                case 26..<51 :
                    massageWebView.evaluateJavaScript("stageTwoBot();")
                    break
                case 51..<76 :
                    massageWebView.evaluateJavaScript("stageThreeBot();")
                    break
                case 76..<101 :
                    massageWebView.evaluateJavaScript("stageFourBot();")
                    break
                default :
                    break
                }
            }
            
        } else {
            topSlider.isEnabled = false
            midSlider.isEnabled = false
            botSlider.isEnabled = false
            massageWebView.evaluateJavaScript("disableModus();")
            // Reset Massage values on BLE board
            BLEDevice.shared.disableMassageTab()
            // Reset GUI values to 0
            topSlider.value = 0
            midSlider.value = 0
            botSlider.value = 0
        }
    }
    
    @IBAction func topSliderChanged(_ sender: Any) {
        let intValueTop:Int = Int(topSlider.value)
        print ("Rückenbereich ist auf \(intValueTop)")
        BLEDevice.shared.writeMassageTopValue(value: intValueTop)
        preferences.set(intValueTop, forKey: "topValue")
        //animate css effect for massage
        switch intValueTop {
        case 0 :
            massageWebView.evaluateJavaScript("stageZeroTop();")
            break
        case 1..<26 :
            massageWebView.evaluateJavaScript("stageOneTop();")
            break
        case 26..<51 :
            massageWebView.evaluateJavaScript("stageTwoTop();")
            break
        case 51..<76 :
            massageWebView.evaluateJavaScript("stageThreeTop();")
            break
        case 76..<101 :
            massageWebView.evaluateJavaScript("stageFourTop();")
            break
        default :
            break
        }
        
    }
    
    @objc func topSliderTapped(gestureRecognizer: UIGestureRecognizer) {
        let pointTapped: CGPoint = gestureRecognizer.location(in: self.view)
        let positionOfSlider: CGPoint = topSlider.frame.origin
        let widthOfSlider: CGFloat = topSlider.frame.size.width
        let newValue = ((pointTapped.x - positionOfSlider.x) * CGFloat(topSlider.maximumValue) / widthOfSlider)
        topSlider.setValue(Float(newValue), animated: true)
        
        // Same arg as in value changed event -> BLE value
        let intValueTop:NSInteger = NSInteger(topSlider.value)
        print ("Massage oben ist auf \(intValueTop)")
        BLEDevice.shared.writeMassageTopValue(value: intValueTop)
        preferences.set(intValueTop, forKey: "topValue")
        
        //animate css effect for massage
        switch intValueTop {
        case 0 :
            massageWebView.evaluateJavaScript("stageZeroTop();")
            break
        case 1..<26 :
            massageWebView.evaluateJavaScript("stageOneTop();")
            break
        case 26..<51 :
            massageWebView.evaluateJavaScript("stageTwoTop();")
            break
        case 51..<76 :
            massageWebView.evaluateJavaScript("stageThreeTop();")
            break
        case 76..<101 :
            massageWebView.evaluateJavaScript("stageFourTop();")
            break
        default :
            break
        }
        
    }
    
    @IBAction func midSliderChanged(_ sender: Any) {
        let intValueMid:Int = Int(midSlider.value)
        print ("Lendenbereich ist auf \(intValueMid)")
        BLEDevice.shared.writeMassageMidValue(value: intValueMid)
        preferences.set(intValueMid, forKey: "midValue")
        
        //animate css effect for massage
        switch intValueMid {
        case 0 :
            massageWebView.evaluateJavaScript("stageZeroMid();")
            break
        case 1..<26 :
            massageWebView.evaluateJavaScript("stageOneMid();")
            break
        case 26..<51 :
            massageWebView.evaluateJavaScript("stageTwoMid();")
            break
        case 51..<76 :
            massageWebView.evaluateJavaScript("stageThreeMid();")
            break
        case 76..<101 :
            massageWebView.evaluateJavaScript("stageFourMid();")
            break
        default :
            break
        }
    }
    
    @objc func midSliderTapped(gestureRecognizer: UIGestureRecognizer) {
        let pointTapped: CGPoint = gestureRecognizer.location(in: self.view)
        let positionOfSlider: CGPoint = midSlider.frame.origin
        let widthOfSlider: CGFloat = midSlider.frame.size.width
        let newValue = ((pointTapped.x - positionOfSlider.x) * CGFloat(midSlider.maximumValue) / widthOfSlider)
        midSlider.setValue(Float(newValue), animated: true)
        
        // Same arg as in value changed event -> BLE value
        let intValueMid:NSInteger = NSInteger(midSlider.value)
        print ("Massage mittig ist auf \(intValueMid)")
        BLEDevice.shared.writeMassageMidValue(value: intValueMid)
        preferences.set(intValueMid, forKey: "midValue")
        
        //animate css effect for massage
        switch intValueMid {
        case 0 :
            massageWebView.evaluateJavaScript("stageZeroMid();")
            break
        case 1..<26 :
            massageWebView.evaluateJavaScript("stageOneMid();")
            break
        case 26..<51 :
            massageWebView.evaluateJavaScript("stageTwoMid();")
            break
        case 51..<76 :
            massageWebView.evaluateJavaScript("stageThreeMid();")
            break
        case 76..<101 :
            massageWebView.evaluateJavaScript("stageFourMid();")
            break
        default :
            break
        }
        
    }
    
    @IBAction func botSliderChanged(_ sender: Any) {
        let intValueBot:Int = Int(botSlider.value)
        print ("Sitzfläche ist auf \(intValueBot)")
        BLEDevice.shared.writeMassageBotValue(value: intValueBot)
        preferences.set(intValueBot, forKey: "botValue")
        
        //animate css effect for massage
        switch intValueBot {
        case 0 :
            massageWebView.evaluateJavaScript("stageZeroBot();")
            break
        case 1..<26 :
            massageWebView.evaluateJavaScript("stageOneBot();")
            break
        case 26..<51 :
            massageWebView.evaluateJavaScript("stageTwoBot();")
            break
        case 51..<76 :
            massageWebView.evaluateJavaScript("stageThreeBot();")
            break
        case 76..<101 :
            massageWebView.evaluateJavaScript("stageFourBot();")
            break
        default :
            break
        }
    }
    
    @objc func botSliderTapped(gestureRecognizer: UIGestureRecognizer) {
        let pointTapped: CGPoint = gestureRecognizer.location(in: self.view)
        let positionOfSlider: CGPoint = botSlider.frame.origin
        let widthOfSlider: CGFloat = botSlider.frame.size.width
        let newValue = ((pointTapped.x - positionOfSlider.x) * CGFloat(botSlider.maximumValue) / widthOfSlider)
        botSlider.setValue(Float(newValue), animated: true)
        
        // Same arg as in value changed event -> BLE value
        let intValueBot:NSInteger = NSInteger(botSlider.value)
        print ("Message unten ist auf \(intValueBot)")
        BLEDevice.shared.writeMassageBotValue(value: intValueBot)
        preferences.set(intValueBot, forKey: "botValue")
        
        //animate css effect for massage
        switch intValueBot {
        case 0 :
            massageWebView.evaluateJavaScript("stageZeroBot();")
            break
        case 1..<26 :
            massageWebView.evaluateJavaScript("stageOneBot();")
            break
        case 26..<51 :
            massageWebView.evaluateJavaScript("stageTwoBot();")
            break
        case 51..<76 :
            massageWebView.evaluateJavaScript("stageThreeBot();")
            break
        case 76..<101 :
            massageWebView.evaluateJavaScript("stageFourBot();")
            break
        default :
            break
        }
    }
    
    // Notification function called when device is connected
    /*@objc func sensorStateChanged(_ notificaiton:Notification) {
        //check sensor state
        if (BLEDevice.shared.sensorState == 0) {
            disconnectedFromSensor()
        }
    }*/
    
    // Notification function called when device gets disconnected
    @objc func lostConnection(_ notificaiton:Notification) {
        disconnected()
    }
    
    func disconnected() {
        //blur background
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.regular)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.view.addSubview(blurEffectView)
        
        popUpView2.center = self.view.center
        popUpView2.layer.cornerRadius = 10
        self.view.addSubview(popUpView2)
        
        // Add a gesture recognizer to the slider
        let tapGestureRecognizerInfo = UITapGestureRecognizer(target: self, action: #selector(infoTapped(gestureRecognizer:)))
        
        let infoView = UIImageView()
        infoView.image = UIImage(named: "questionMark")
        infoView.frame = CGRect(x: self.view.frame.width - 50, y: 30, width: 30, height: 45)
        infoView.addGestureRecognizer(tapGestureRecognizerInfo)
        infoView.isUserInteractionEnabled = true
        self.view.addSubview(infoView)
        
        tabBarController?.tabBar.isHidden = true
    }
    
    @objc func infoTapped(gestureRecognizer: UIGestureRecognizer) {
        //get current language
        let language = Locale.current.languageCode
        
        switch (language) {
        case "de":
            guard let url = URL(string: "https://www.kloeber.com/de/de/wooom/") else { return }
            UIApplication.shared.open(url)
            break
        case "en":
            guard let url = URL(string: "https://www.kloeber.com/kl/en/wooom/") else { return }
            UIApplication.shared.open(url)
            break
        case "fr":
            guard let url = URL(string: "https://www.kloeber.com/fr/fr/wooom/") else { return }
            UIApplication.shared.open(url)
            break
        case "nl":
            guard let url = URL(string: "https://www.kloeber.com/nl/nl/wooom/") else { return }
            UIApplication.shared.open(url)
            break
        default:
            guard let url = URL(string: "https://www.kloeber.com/kl/en/wooom/") else { return }
            UIApplication.shared.open(url)
            break
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

