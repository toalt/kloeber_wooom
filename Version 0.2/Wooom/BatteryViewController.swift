//
//  BatteryViewController.swift
//  Wooom
//
//  Created by Tobias Alt on 10.08.18.
//  Copyright © 2018 Kloeber. All rights reserved.
//

import UIKit
import WebKit

class BatteryViewController: UIViewController,WKUIDelegate {

    // Reference WKWebView
    var batteryWebView: WKWebView!
    
    // Notication Center for Update characteristics
    let nc = NotificationCenter.default
    
    // IBOutlets
    @IBOutlet weak var webContainer: UIView!
    @IBOutlet weak var batteryProgress: UIProgressView!
    @IBOutlet weak var progressInPercentage: UILabel!
    @IBOutlet weak var timeRemaining: UILabel!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // WKWebView setup
        let webConfiguration = WKWebViewConfiguration()
        // Init WKWebView
        batteryWebView = WKWebView(frame: webContainer.bounds, configuration: webConfiguration)
        batteryWebView.uiDelegate = self
        // Add to container
        webContainer.addSubview(batteryWebView)
        
        // Constraints to fit container layout
        batteryWebView.translatesAutoresizingMaskIntoConstraints = false
        batteryWebView.leadingAnchor.constraint(equalTo: webContainer.leadingAnchor, constant: 0).isActive = true
        batteryWebView.trailingAnchor.constraint(equalTo: webContainer.trailingAnchor, constant: 0).isActive = true
        batteryWebView.topAnchor.constraint(equalTo: webContainer.topAnchor, constant: 0).isActive = true
        batteryWebView.bottomAnchor.constraint(equalTo: webContainer.bottomAnchor, constant: 0).isActive = true
        
        // Load WKWebView resources
        let htmlPath = Bundle.main.path(forResource: "battery", ofType: "html")
        let htmlUrl = URL(fileURLWithPath: htmlPath!, isDirectory: false)
        batteryWebView.loadFileURL(htmlUrl, allowingReadAccessTo: htmlUrl)
        // Let WebView appear transparent and fit full content
        batteryWebView.isOpaque = false
        batteryWebView.backgroundColor = UIColor.clear
        batteryWebView.scrollView.bounces = false
        batteryWebView.scrollView.contentInsetAdjustmentBehavior = .never

        // First notification observs is missed -> set voltage via singleton property
        if (BLEDevice.shared.isConnected == true) {
            if(BLEDevice.shared.voltage != nil) {
                setBatteryProgress(value: BLEDevice.shared.voltage)
            }
            if((BLEDevice.shared.remainHours != nil) && (BLEDevice.shared.remainMin != nil)) {
                setBatteryTimeRemaining(hours: BLEDevice.shared.remainHours, min: BLEDevice.shared.remainMin)
            }
        }
        // Add Observer for listing to battery voltage characteristc
        nc.addObserver(self, selector: #selector(voltageDidUpdate(_:)), name: Notification.Name("VoltageUpdate"), object: nil)
        nc.addObserver(self, selector: #selector(remainDidUpdate(_:)), name: Notification.Name("TimeRemainUpdate"), object: nil)
        
    }
    
    // Notification function called each time BLEDevice gets characteristc update
    @objc func voltageDidUpdate(_ notificaiton:Notification) {
        setBatteryProgress(value: BLEDevice.shared.voltage)
    }
    
    // Notification function called each time BLEDevice gets characteristc update
    @objc func remainDidUpdate(_ notificaiton:Notification) {
        setBatteryTimeRemaining(hours: 2, min: 00)
    }
    
    func setBatteryProgress(value: Int) {
        batteryProgress.progress = Float(value) / 100
        progressInPercentage.text = String(value) + "%"
    }
    
    func setBatteryTimeRemaining (hours: Int, min: Int) {
        if (min < 10) {
            timeRemaining.text = "Restlaufzeit beträgt \(hours) h 0\(min) min"
        } else {
            timeRemaining.text = "Restlaufzeit beträgt \(hours) h \(min) min"
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
