//
//  NotificationViewController.swift
//  Wooom
//
//  Created by Tobias Alt on 10.08.18.
//  Copyright © 2018 Kloeber. All rights reserved.
//

import UIKit
import WebKit
import CoreNFC

class NotificationViewController: UIViewController,WKUIDelegate,WKScriptMessageHandler,NFCNDEFReaderSessionDelegate {
   
    // Reference the NFC session
    var nfcSession: NFCNDEFReaderSession!
    
    // Reference WKWebView
    var notificationWebView: WKWebView!
    
    // Notication Center for update device state
    let nc = NotificationCenter.default
    
    // Name of the BLE device (scanned by NFC Reader)
    var bleDeviceName: String!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let contentController = WKUserContentController()
        contentController.add(self, name: "nfcScan")
        contentController.add(self, name: "connection")
        
        let webConfiguration = WKWebViewConfiguration()
        webConfiguration.userContentController = contentController
        
        notificationWebView = WKWebView(frame: view.bounds, configuration: webConfiguration)
        notificationWebView.uiDelegate = self
        
        view.addSubview(notificationWebView)
        
        // Load WKWebView resources
        let htmlPath = Bundle.main.path(forResource: "notificationDE", ofType: "html")
        let htmlUrl = URL(fileURLWithPath: htmlPath!, isDirectory: false)
        notificationWebView.loadFileURL(htmlUrl, allowingReadAccessTo: htmlUrl)

        notificationWebView.isOpaque = false
        notificationWebView.backgroundColor = UIColor.clear
        notificationWebView.scrollView.bounces = false
        notificationWebView.scrollView.contentInsetAdjustmentBehavior = .never
        
        // Add Observer for listing to battery voltage characteristc
        nc.addObserver(self, selector: #selector(connectionEstablished(_:)), name: Notification.Name("BLEConnected"), object: nil)
    }
    
    // JS Bridge Observer for messages -> name is identifier not message
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        if (message.name == "nfcScan") {
            // NFC Session
            nfcSession = NFCNDEFReaderSession(delegate: self, queue: DispatchQueue.main, invalidateAfterFirstRead: true)
            self.nfcSession.alertMessage = "Halten Sie Ihr iPhone mit der Rückseite auf den NFC-Tag"
            self.nfcSession.begin()
        }
        if (message.name == "connection") {
            DispatchQueue.main.asyncAfter(deadline: .now()+3) { //desired number of seconds
                self.performSegue(withIdentifier: "notificationToApp", sender: nil)
            }
        }
    }

    // NFC Reader Session
    func readerSession(_ session: NFCNDEFReaderSession, didInvalidateWithError error: Error) {
        print("Error reading NFC: \(error.localizedDescription)")
    }
    
    func readerSession(_ session: NFCNDEFReaderSession, didDetectNDEFs messages: [NFCNDEFMessage]) {
        for message in messages {
            for record in message.records {
                if let string = String(data: record.payload, encoding: .ascii) {
                    bleDeviceName = string
                    print("------BLE DERVICE NAME FROM TAG -----")
                    print(bleDeviceName)
                    // Notify BLEDevice singleton about the device name from NFC Tag
                    BLEDevice.shared.fetchBLEDeviceName(name: bleDeviceName)
                }
            }
        }
        BLEDevice.shared.initCBManager()
        // Time to wait until NFC dialog disappear
        sleep(UInt32(3.0))
        notificationWebView.evaluateJavaScript("activityIndicator()")
        // From here the controller waits for the connection status from Singleton via Notification center
    }
    
    // Notification function called when device is connected
    @objc func connectionEstablished(_ notificaiton:Notification) {
        notificationWebView.evaluateJavaScript("connSuccess()")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
