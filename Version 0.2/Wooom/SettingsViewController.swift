//
//  SettingsViewController.swift
//  Wooom
//
//  Created by Tobias Alt on 10.08.18.
//  Copyright © 2018 Kloeber. All rights reserved.
//

import UIKit
import WebKit

class SettingsViewController: UIViewController,WKUIDelegate {

    // Reference WKWebView
    var settingsWebView: WKWebView!
    
    // Corparate color
    let primaryColor = UIColor(rgb: 0xFFBB02)
    
    // IBOutlets
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var connectinState: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let webConfiguration = WKWebViewConfiguration()
        
        settingsWebView = WKWebView(frame: view.bounds, configuration: webConfiguration)
        settingsWebView.uiDelegate = self
        
        //view.addSubview(settingsWebView)
        
        // Load WKWebView resources
        let htmlPath = Bundle.main.path(forResource: "settingsDE", ofType: "html")
        let htmlUrl = URL(fileURLWithPath: htmlPath!, isDirectory: false)
        settingsWebView.loadFileURL(htmlUrl, allowingReadAccessTo: htmlUrl)
        
        settingsWebView.isOpaque = false
        settingsWebView.backgroundColor = UIColor.clear
        settingsWebView.scrollView.bounces = false
        settingsWebView.scrollView.contentInsetAdjustmentBehavior = .never
        
        // write ID to settings label 
        setId()
    }
    
    func setId() {
        if(BLEDevice.shared.isConnected == true) {
            connectinState.text = "Stuhl verbunden"
            idLabel.text = BLEDevice.shared.targetDeviceName
        } else {
            connectinState.text = "Stuhl nicht verbunden"
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}


