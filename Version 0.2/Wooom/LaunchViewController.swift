//
//  LaunchViewController.swift
//  Wooom
//
//  Created by Tobias Alt on 10.08.18.
//  Copyright © 2018 Kloeber. All rights reserved.
//

import UIKit
import WebKit

class LaunchViewController: UIViewController,WKUIDelegate {
    
    var launchWebView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let webConfiguration = WKWebViewConfiguration()
        
        launchWebView = WKWebView(frame: view.bounds, configuration: webConfiguration)
        launchWebView.uiDelegate = self
        
        view.addSubview(launchWebView)
        
        // Load WKWebView resources
        let htmlPath = Bundle.main.path(forResource: "launchDE", ofType: "html")
        let htmlUrl = URL(fileURLWithPath: htmlPath!, isDirectory: false)
        launchWebView.loadFileURL(htmlUrl, allowingReadAccessTo: htmlUrl)
        
        // Make background transparant and fit full screen
        launchWebView.isOpaque = false
        launchWebView.backgroundColor = UIColor.clear
        launchWebView.scrollView.bounces = false
        launchWebView.scrollView.contentInsetAdjustmentBehavior = .never
        
        //show screen 2 sec before enter app
        DispatchQueue.main.asyncAfter(deadline: .now()+2) { //desired number of seconds
            self.performSegue(withIdentifier: "launchToNotification", sender: nil)
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
