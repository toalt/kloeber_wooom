//
//  TemperatureViewController.swift
//  Wooom
//
//  Created by Tobias Alt on 10.08.18.
//  Copyright © 2018 Kloeber. All rights reserved.
//

import UIKit
import WebKit

class TemperatureViewController: UIViewController,WKUIDelegate,WKScriptMessageHandler {

    // WKWebView for chair SVG
    var tempWebView: WKWebView!
    
    // IBOutlets
    @IBOutlet weak var switchState: UISwitch!
    @IBOutlet weak var webContainer: UIView!
    @IBOutlet weak var sitSlider: UISlider!
    @IBOutlet weak var sitLabel: UILabel!
    @IBOutlet weak var backSlider: UISlider!
    @IBOutlet weak var backLabel: UILabel!
    
    
    //corparate color
    let primaryColor = UIColor(rgb: 0xFFBB02)
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Change background color of the switch
        switchState.backgroundColor = UIColor.gray
        switchState.layer.cornerRadius = 16.0
        
        // Set slider thumb image
        sitSlider.setThumbImage(#imageLiteral(resourceName: "thumb"), for: .normal)
        backSlider.setThumbImage(#imageLiteral(resourceName: "thumb"), for: .normal)
        
        
        // WKWebView setup
        let webConfiguration = WKWebViewConfiguration()
        let contentController = WKUserContentController()
        //message name for js bridge
        contentController.add(self, name: "boost")
        contentController.add(self, name: "boostEnd")
        webConfiguration.userContentController = contentController
        // Init WKWebView
        tempWebView = WKWebView(frame: webContainer.bounds, configuration: webConfiguration)
        tempWebView.uiDelegate = self
        // Add to container
        webContainer.addSubview(tempWebView)
        
        // Constraints to fit container layout
        tempWebView.translatesAutoresizingMaskIntoConstraints = false
        tempWebView.leadingAnchor.constraint(equalTo: webContainer.leadingAnchor, constant: 0).isActive = true
        tempWebView.trailingAnchor.constraint(equalTo: webContainer.trailingAnchor, constant: 0).isActive = true
        tempWebView.topAnchor.constraint(equalTo: webContainer.topAnchor, constant: 0).isActive = true
        tempWebView.bottomAnchor.constraint(equalTo: webContainer.bottomAnchor, constant: 0).isActive = true
        
        // Load WKWebView resources
        let htmlPath = Bundle.main.path(forResource: "temperature", ofType: "html")
        let htmlUrl = URL(fileURLWithPath: htmlPath!, isDirectory: false)
        tempWebView.loadFileURL(htmlUrl, allowingReadAccessTo: htmlUrl)
        // Let WebView appear transparent and fit full content
        tempWebView.isOpaque = false
        tempWebView.backgroundColor = UIColor.clear
        tempWebView.scrollView.bounces = false
        tempWebView.scrollView.contentInsetAdjustmentBehavior = .never
    }
    
    // JS Bridge Observer for messages -> name is identifier not message
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        if (message.name == "boost") {
            //print("Boost aktiviert")
            sitSlider.value = 100
            backSlider.value = 100
            BLEDevice.shared.startBoost()
        }
        if (message.name == "boostEnd") {
            //print("Boost beendet")
            sitSlider.value = 0
            backSlider.value = 0
            BLEDevice.shared.endBoost()
        }
    }
    
    
    @IBAction func switchStateChanged(_ sender: Any) {
        if (switchState.isOn) {
            sitSlider.isEnabled = true
            backSlider.isEnabled = true
            sitLabel.textColor = primaryColor
            backLabel.textColor = primaryColor
            // Enable webview button -> boost
            tempWebView.evaluateJavaScript("enableButton();")
            // Load last values from BLE board            
        } else {
            sitSlider.isEnabled = false
            backSlider.isEnabled = false
            sitLabel.textColor = UIColor.white
            backLabel.textColor = UIColor.white
            // Disable webview button -> boost
            tempWebView.evaluateJavaScript("disableButton();")
            // Interrupt boost while state -> OFF
            tempWebView.evaluateJavaScript("interruptBoost();")
            // Reset Temperature values on BLE board (Boost included -> delegated from boostEnd event via JS)
            BLEDevice.shared.disableTemperatureTab()
        }
    }
    
    @IBAction func sitValueChanged(_ sender: Any) {
        let intValueSit:Int = Int(sitSlider.value)
        print ("Sitzen ist auf \(intValueSit)")
        BLEDevice.shared.writeHeatingSitValue(value: intValueSit)
    }

    @IBAction func backValueChanged(_ sender: Any) {
        let intValueBack:Int = Int(backSlider.value)
        print ("Rücken ist auf \(intValueBack)")
        BLEDevice.shared.writeHeatingBackValue(value: intValueBack)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

