//
//  FirstViewController.swift
//  Wooom
//
//  Created by Tobias Alt on 07.08.18.
//  Copyright © 2018 Kloeber. All rights reserved.
//

/////////////////////////////////////////////////////////////////////////////////////////
// History: V 0.1: Project start and testing for upload to iTunes Connect              //
//          V 0.2: Prototype version for Kloeber fair                                  //
//                 BlE functions enabled -> read/write                                 //
//                 No animations, no warnings, no custom user prefrences (last values) //
/////////////////////////////////////////////////////////////////////////////////////////


import UIKit
import WebKit

class LightViewController: UIViewController,WKUIDelegate {

    // WKWebView for chair SVG
    var lightWebView: WKWebView!
    
    // IBOutlets
    @IBOutlet weak var stateSwitch: UISwitch!
    @IBOutlet weak var webContainer: UIView!
    @IBOutlet weak var dimSlider: UISlider!
    @IBOutlet weak var dimLabel: UILabel!
    @IBOutlet weak var tempSlider: UISlider!
    @IBOutlet weak var tempLabel: UILabel!
    
    //corparate color
    let primaryColor = UIColor(rgb: 0xFFBB02)

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Color the switch background
        stateSwitch.backgroundColor = UIColor.gray
        stateSwitch.layer.cornerRadius = 16.0
        
        // Set slider thumb image
        dimSlider.setThumbImage(#imageLiteral(resourceName: "thumb"), for: .normal)
        tempSlider.setThumbImage(#imageLiteral(resourceName: "thumb"), for: .normal)
        
        // WKWebView setup
        let webConfiguration = WKWebViewConfiguration()
        // Init WKWebView
        lightWebView = WKWebView(frame: webContainer.bounds, configuration: webConfiguration)
        lightWebView.uiDelegate = self
        // Add to container
        webContainer.addSubview(lightWebView)
        
        // Constraints to fit container layout
        lightWebView.translatesAutoresizingMaskIntoConstraints = false
        lightWebView.leadingAnchor.constraint(equalTo: webContainer.leadingAnchor, constant: 0).isActive = true
        lightWebView.trailingAnchor.constraint(equalTo: webContainer.trailingAnchor, constant: 0).isActive = true
        lightWebView.topAnchor.constraint(equalTo: webContainer.topAnchor, constant: 0).isActive = true
        lightWebView.bottomAnchor.constraint(equalTo: webContainer.bottomAnchor, constant: 0).isActive = true
        
        // Load WKWebView resources
        let htmlPath = Bundle.main.path(forResource: "light", ofType: "html")
        let htmlUrl = URL(fileURLWithPath: htmlPath!, isDirectory: false)
        lightWebView.loadFileURL(htmlUrl, allowingReadAccessTo: htmlUrl)
        // Let WebView appear transparent and fit full content
        lightWebView.isOpaque = false
        lightWebView.backgroundColor = UIColor.clear
        lightWebView.scrollView.bounces = false
        lightWebView.scrollView.contentInsetAdjustmentBehavior = .never
        
    }
    
    @IBAction func stateSwitchChanged(_ sender: Any) {
        if (stateSwitch.isOn) {
            dimSlider.isEnabled = true
            tempSlider.isEnabled = true
            dimLabel.textColor = primaryColor
            tempLabel.textColor = primaryColor
            lightWebView.evaluateJavaScript("enableLight();")
            // Load last values from BLE board
        } else {
            dimSlider.isEnabled = false
            tempSlider.isEnabled = false
            dimLabel.textColor = UIColor.white
            tempLabel.textColor = UIColor.white
            lightWebView.evaluateJavaScript("disableLight();")
            // Reset LED values on BLE board
            BLEDevice.shared.disableLightTab()
            // Reset GUI values to 0
            dimSlider.value = 0
            tempSlider.value = 0
        }
    }
    
    @IBAction func dimValueChanged(_ sender: Any) {
        let intValueDim:NSInteger = NSInteger(dimSlider.value)
        print ("Dimmer ist auf \(intValueDim)")
        BLEDevice.shared.writeLedBrightValue(value: intValueDim)
    }
    
    @IBAction func tempValueChanged(_ sender: Any) {
        let intValueTemp:Int = Int(tempSlider.value)
        print ("Temperatur ist auf \(intValueTemp)")
        BLEDevice.shared.writeLedColorValue(value: intValueTemp)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

//convert hex color #
extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(rgb: Int) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF
        )
    }
}
