var chair = document.getElementById("chair");
var boostBtn = document.getElementById("boost");
//grab back area circles
var backOuter = document.getElementById("backArea");
var backCircleOne = document.getElementById("backCircleOne");
var backCircleTwo = document.getElementById("backCircleTwo");
var backCircleThree = document.getElementById("backCircleThree");
//grab sit area circles
var sitOuter = document.getElementById("sitArea");
var sitCircleOne = document.getElementById("sitCircleOne");
var sitCircleTwo = document.getElementById("sitCircleTwo");
var sitCircleThree = document.getElementById("sitCircleThree");

//top pos of the first circle area
var topPos = backOuter.offsetTop;
var standardPos = topPos;
var posModTwo = topPos-2;
var posModThree = topPos-4;
var posModFour = topPos-6;

//top pos of the second circle area
var topPosSit = sitOuter.offsetTop;
var standardPosSit = topPosSit;
var posModTwoSit = topPosSit-2;
var posModThreeSit = topPosSit-4;
var posModFourSit = topPosSit-6;

//variable to interrupt
var timeout;
var colorInterval;
//counter variable
var countColor = 0;
var countFont = 0;


//iOS Switch Button
function enableButton() {
    boostBtn.disabled = false;
    chair.style.opacity = 0.9;
    backStageZero();
    sitStageZero();
}

function disableButton() {
    boostBtn.disabled = true;
    chair.style.opacity = 0.3;
    backOuter.style.border = "1px solid grey";
    backCircleOne.style.border = "1px solid grey";
    backCircleTwo.style.border = "1px solid grey";
    backCircleThree.style.border = "1px solid grey";
    //calc new because of border size
    backOuter.style.marginLeft = "-55px";
    backOuter.style.top = standardPos+"px";
    backCircleOne.style.top = "12px";
    backCircleTwo.style.top = "12px";
    backCircleThree.style.top = "12px";
    backCircleOne.style.left = "12px";
    backCircleTwo.style.left = "12px";
    backCircleThree.style.left = "12px";
    //second circle area
    sitOuter.style.border = "1px solid grey";
    sitCircleOne.style.border = "1px solid grey";
    sitCircleTwo.style.border = "1px solid grey";
    sitCircleThree.style.border = "1px solid grey";
    //calc new because of border size
    sitOuter.style.marginLeft = "-55px";
    sitOuter.style.top = standardPosSit+"px";
    sitCircleOne.style.top = "12px";
    sitCircleTwo.style.top = "12px";
    sitCircleThree.style.top = "12px";
    sitCircleOne.style.left = "12px";
    sitCircleTwo.style.left = "12px";
    sitCircleThree.style.left = "12px";
}

function backStageZero() {
    backOuter.style.border = "1px solid white";
    backCircleOne.style.border = "1px solid white";
    backCircleTwo.style.border = "1px solid white";
    backCircleThree.style.border = "1px solid white";
    //calc new because of border size
    backOuter.style.marginLeft = "-55px";
    backOuter.style.top = standardPos+"px";
    backCircleOne.style.top = "12px";
    backCircleTwo.style.top = "12px";
    backCircleThree.style.top = "12px";
    backCircleOne.style.left = "12px";
    backCircleTwo.style.left = "12px";
    backCircleThree.style.left = "12px";
}

//Temparature effect - Stage at 1-25 %
function backStageOne() {
    backOuter.style.border = "1px solid #FFBB02";
    backCircleOne.style.border = "1px solid #FFBB02";
    backCircleTwo.style.border = "1px solid #FFBB02";
    backCircleThree.style.border = "1px solid #FFBB02";
    //calc new because of border size
    backOuter.style.marginLeft = "-55px";
    backOuter.style.top = standardPos+"px";
    backCircleOne.style.top = "12px";
    backCircleTwo.style.top = "12px";
    backCircleThree.style.top = "12px";
    backCircleOne.style.left = "12px";
    backCircleTwo.style.left = "12px";
    backCircleThree.style.left = "12px";
}

//Stage 26-50 %
function backStageTwo() {
    backOuter.style.border = "3px solid #FFBB02";
    backCircleOne.style.border = "3px solid #FFBB02";
    backCircleTwo.style.border = "3px solid #FFBB02";
    backCircleThree.style.border = "3px solid #FFBB02";
    //calc new because of border size
    backOuter.style.marginLeft = "-57px";
    backOuter.style.top = posModTwo+"px";
    backCircleOne.style.top = "10px";
    backCircleTwo.style.top = "10px";
    backCircleThree.style.top = "10px";
    backCircleOne.style.left = "10px";
    backCircleTwo.style.left = "10px";
    backCircleThree.style.left = "10px";
}

//Stage 51-75
function backStageThree() {
    backOuter.style.border = "5px solid #FFBB02";
    backCircleOne.style.border = "5px solid #FFBB02";
    backCircleTwo.style.border = "5px solid #FFBB02";
    backCircleThree.style.border = "5px solid #FFBB02";
    //calc new because of border size
    backOuter.style.marginLeft = "-59px";
    backOuter.style.top = posModThree+"px";
    backCircleOne.style.top = "8px";
    backCircleTwo.style.top = "8px";
    backCircleThree.style.top = "8px";
    backCircleOne.style.left = "8px";
    backCircleTwo.style.left = "8px";
    backCircleThree.style.left = "8px";
}

//Stage 76-100
function backStageFour() {
    backOuter.style.border = "7px solid #FFBB02";
    backCircleOne.style.border = "7px solid #FFBB02";
    backCircleTwo.style.border = "7px solid #FFBB02";
    backCircleThree.style.border = "7px solid #FFBB02";
    //calc new because of border size
    backOuter.style.marginLeft = "-61px";
    backOuter.style.top = posModFour+"px";
    backCircleOne.style.top = "6px";
    backCircleTwo.style.top = "6px";
    backCircleThree.style.top = "6px";
    backCircleOne.style.left = "6px";
    backCircleTwo.style.left = "6px";
    backCircleThree.style.left = "6px";
}

function sitStageZero() {
    sitOuter.style.border = "1px solid white";
    sitCircleOne.style.border = "1px solid white";
    sitCircleTwo.style.border = "1px solid white";
    sitCircleThree.style.border = "1px solid white";
    //calc new because of border size
    sitOuter.style.marginLeft = "-55px";
    sitOuter.style.top = standardPosSit+"px";
    sitCircleOne.style.top = "12px";
    sitCircleTwo.style.top = "12px";
    sitCircleThree.style.top = "12px";
    sitCircleOne.style.left = "12px";
    sitCircleTwo.style.left = "12px";
    sitCircleThree.style.left = "12px";
}

//Temparature effect - Stage at 1-25 %
function sitStageOne() {
    sitOuter.style.border = "1px solid #FFBB02";
    sitCircleOne.style.border = "1px solid #FFBB02";
    sitCircleTwo.style.border = "1px solid #FFBB02";
    sitCircleThree.style.border = "1px solid #FFBB02";
    //calc new because of border size
    sitOuter.style.marginLeft = "-55px";
    sitOuter.style.top = standardPosSit+"px";
    sitCircleOne.style.top = "12px";
    sitCircleTwo.style.top = "12px";
    sitCircleThree.style.top = "12px";
    sitCircleOne.style.left = "12px";
    sitCircleTwo.style.left = "12px";
    sitCircleThree.style.left = "12px";
}

//Stage 26-50 %
function sitStageTwo() {
    sitOuter.style.border = "3px solid #FFBB02";
    sitCircleOne.style.border = "3px solid #FFBB02";
    sitCircleTwo.style.border = "3px solid #FFBB02";
    sitCircleThree.style.border = "3px solid #FFBB02";
    //calc new because of border size
    sitOuter.style.marginLeft = "-57px";
    sitOuter.style.top = posModTwoSit+"px";
    sitCircleOne.style.top = "10px";
    sitCircleTwo.style.top = "10px";
    sitCircleThree.style.top = "10px";
    sitCircleOne.style.left = "10px";
    sitCircleTwo.style.left = "10px";
    sitCircleThree.style.left = "10px";
}

//Stage 51-75
function sitStageThree() {
    sitOuter.style.border = "5px solid #FFBB02";
    sitCircleOne.style.border = "5px solid #FFBB02";
    sitCircleTwo.style.border = "5px solid #FFBB02";
    sitCircleThree.style.border = "5px solid #FFBB02";
    //calc new because of border size
    sitOuter.style.marginLeft = "-59px";
    sitOuter.style.top = posModThreeSit+"px";
    sitCircleOne.style.top = "8px";
    sitCircleTwo.style.top = "8px";
    sitCircleThree.style.top = "8px";
    sitCircleOne.style.left = "8px";
    sitCircleTwo.style.left = "8px";
    sitCircleThree.style.left = "8px";
}

//Stage 76-100
function sitStageFour() {
    sitOuter.style.border = "7px solid #FFBB02";
    sitCircleOne.style.border = "7px solid #FFBB02";
    sitCircleTwo.style.border = "7px solid #FFBB02";
    sitCircleThree.style.border = "7px solid #FFBB02";
    //calc new because of border size
    sitOuter.style.marginLeft = "-61px";
    sitOuter.style.top = posModFourSit+"px";
    sitCircleOne.style.top = "6px";
    sitCircleTwo.style.top = "6px";
    sitCircleThree.style.top = "6px";
    sitCircleOne.style.left = "6px";
    sitCircleTwo.style.left = "6px";
    sitCircleThree.style.left = "6px";
}

function hideCircles() {
    backOuter.style.visibility = "hidden";
    sitOuter.style.visibility = "hidden";
}

function showCircles() {
    backOuter.style.visibility = "visible";
    sitOuter.style.visibility = "visible";
}


//Boost
function boostUp() {
    hideCircles();
    chair.style.background = "url(boost.svg)";
    chair.style.backgroundRepeat = "no-repeat";
    chair.style.backgroundPosition = "center center";
    //send to native controller
    window.webkit.messageHandlers.boost.postMessage("Boost");
    
    //set background color on start boost
    boostBtn.style.background = "#FFBB02";
    boostBtn.style.fontWeight = "bolder";
    colorInterval = setInterval(changeColor, 750);
    
    //disable button while boost process - remove highlighted style
    boostBtn.disabled = true;
    timeout = setTimeout(function(){
        clearInterval(colorInterval);
        chair.style.background = "url(chair.svg)";
        chair.style.backgroundRepeat = "no-repeat";
        chair.style.backgroundPosition = "center center";
        boostBtn.disabled = false;
        boostBtn.style.background = "transparent";
        boostBtn.style.fontWeight = "normal";
        showCircles();
        window.webkit.messageHandlers.boostEnd.postMessage("Boost beendet");
    },30000)
}

function interruptBoost() {
    clearTimeout(timeout);
    chair.style.background = "url(chair.svg)";
    chair.style.backgroundRepeat = "no-repeat";
    chair.style.backgroundPosition = "center center";
    boostBtn.disabled = true;
    boostBtn.style.background = "transparent";
    boostBtn.style.fontWeight = "normal";
    clearInterval(colorInterval);
    showCircles();
    window.webkit.messageHandlers.boostInterrupt.postMessage("Boost interrupted");
}

function changeColor() {
    var color = ["transparent", "#FFBB02"];
    var font = ["normal", "bolder"];
    boostBtn.style.backgroundColor = color[countColor];
    boostBtn.style.fontWeight = color[countFont];
    countColor = (countColor + 1) % color.length;
    countFont = (countFont + 1) % font.length;
}
