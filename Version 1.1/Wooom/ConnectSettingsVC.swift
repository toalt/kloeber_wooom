//
//  ConnectSettingsVC.swift
//  Wooom
//
//  Created by Tobias Alt on 12.09.18.
//  Copyright © 2018 Kloeber. All rights reserved.
//

import UIKit

class ConnectSettingsVC: UIViewController {

    // IBOutlet
    @IBOutlet weak var labelClick: UILabel!
    @IBOutlet weak var disConView: UIView!
    @IBOutlet weak var arrow: UIImageView!
    
    // Create a global instance of NSUserDefaults class
    let preferences = UserDefaults.standard
    
    // Notication Center for update device state
    let nc = NotificationCenter.default
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Add onclick event to each view
        let tapGestureRecognizerBack = UITapGestureRecognizer(target: self, action: #selector(onBackTapped(gestureRecognizer:)))
        let tapGestureRecognizerArrow = UITapGestureRecognizer(target: self, action: #selector(onBackTapped(gestureRecognizer:)))
        labelClick.addGestureRecognizer(tapGestureRecognizerBack)
        arrow.addGestureRecognizer(tapGestureRecognizerArrow)
        
        let tapGestureRecognizerDiscon = UITapGestureRecognizer(target: self, action: #selector(onDisConTapped(gestureRecognizer:)))
        disConView.addGestureRecognizer(tapGestureRecognizerDiscon)
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @objc func onBackTapped(gestureRecognizer: UIGestureRecognizer) {
        dismiss(animated: true, completion: nil)
    }
    
    @objc func onDisConTapped(gestureRecognizer: UIGestureRecognizer) {
        // delete shared preferences
        BLEDevice.shared.disconnectBLEDevice()
        preferences.removeObject(forKey: "pairedDevice")
        preferences.removeObject(forKey: "version")
        self.performSegue(withIdentifier: "resetButton", sender: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
