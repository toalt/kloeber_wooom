//
//  NotificationViewController.swift
//  Wooom
//
//  Created by Tobias Alt on 10.08.18.
//  Copyright © 2018 Kloeber. All rights reserved.
//

import UIKit
import WebKit
import CoreNFC

class NotificationViewController: UIViewController,WKUIDelegate,WKScriptMessageHandler,NFCNDEFReaderSessionDelegate {
   
    // Reference the NFC session
    var nfcSession: NFCNDEFReaderSession!
    
    @IBOutlet weak var popUpView: UIView!
    @IBOutlet weak var reconBtn: UIButton!
    // Reference WKWebView
    var notificationWebView: WKWebView!
    
    // Notication Center for update device state
    let nc = NotificationCenter.default
    
    // Name of the BLE device (scanned by NFC Reader)
    var bleDeviceName: String!
    
    // Create a global instance of NSUserDefaults class
    let preferences = UserDefaults.standard
    
    //corparate color
    let primaryColor = UIColor(rgb: 0xFFBB02)
    
    var language: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let contentController = WKUserContentController()
        contentController.add(self, name: "nfcScan")
        contentController.add(self, name: "connection")
        
        let webConfiguration = WKWebViewConfiguration()
        webConfiguration.userContentController = contentController
        
        notificationWebView = WKWebView(frame: view.bounds, configuration: webConfiguration)
        notificationWebView.uiDelegate = self
        
        view.addSubview(notificationWebView)
        
        var htmlPath: String?
        
        //get current language
        language = Locale.current.languageCode
        print("LANGUEAGE KEY", language)

        
        switch (language) {
        case "de":
            htmlPath = Bundle.main.path(forResource: "notificationDE", ofType: "html")
            break
        case "en":
            htmlPath = Bundle.main.path(forResource: "notificationEN", ofType: "html")
            break
        case "fr":
            htmlPath = Bundle.main.path(forResource: "notificationFR", ofType: "html")
            break
        case "nl":
            htmlPath = Bundle.main.path(forResource: "notificationNL", ofType: "html")
            break
        default:
            htmlPath = Bundle.main.path(forResource: "notificationEN", ofType: "html")
            break
        }
        
        // Load WKWebView resources
        let htmlUrl = URL(fileURLWithPath: htmlPath!, isDirectory: false)
        notificationWebView.loadFileURL(htmlUrl, allowingReadAccessTo: htmlUrl)

        notificationWebView.isOpaque = false
        notificationWebView.backgroundColor = UIColor.clear
        notificationWebView.scrollView.bounces = false
        notificationWebView.scrollView.contentInsetAdjustmentBehavior = .never
        
        // Add Observer for listing to battery voltage characteristc
        nc.addObserver(self, selector: #selector(connectionEstablished(_:)), name: Notification.Name("BLEConnected"), object: nil)
        nc.addObserver(self, selector: #selector(timeOutNotification(_:)), name: Notification.Name("Timeout"), object: nil)
        
        // Style for blur effect when disconnected
        reconBtn.backgroundColor = primaryColor
        reconBtn.layer.cornerRadius = 10
    }
    
  // hide whole vc duo to unwind ui bug
  override func viewDidDisappear(_ animated: Bool) {
        self.view.isHidden = true
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // JS Bridge Observer for messages -> name is identifier not message
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        if (message.name == "nfcScan") {
            // NFC Session
            if NFCNDEFReaderSession.readingAvailable {
                nfcSession = NFCNDEFReaderSession(delegate: self, queue: DispatchQueue.main, invalidateAfterFirstRead: true)
                switch (language) {
                case "de":
                    self.nfcSession.alertMessage = "Halten Sie Ihr iPhone mit der Rückseite auf den NFC-Tag"
                    break
                case "en":
                    self.nfcSession.alertMessage = "Point the back of your iPhone towards the NFC tag"
                    break
                case "fr":
                    self.nfcSession.alertMessage = "Tenez votre smartphone contre l’étiquette de communication en champ proche (CCP) qui se trouve sur l’accoudoir gauche"
                    break
                case "nl":
                    self.nfcSession.alertMessage = "Houd uw smartphone bij de NFC-tag, die zich in de linker armleuning bevindt"
                    break
                default:
                    self.nfcSession.alertMessage = "Point the back of your iPhone towards the NFC tag"
                    break
                }
                self.nfcSession.begin()
                
            }
        }
        if (message.name == "connection") {
            DispatchQueue.main.asyncAfter(deadline: .now()+3) { //desired number of seconds
                //check product version
                if (BLEDevice.shared.productType == 2) {
                    self.performSegue(withIdentifier: "notificationToVersion", sender: nil)
                } else {
                    self.performSegue(withIdentifier: "notificationToApp", sender: nil)
                }
            }
        }
    }

    // NFC Reader Session
    func readerSession(_ session: NFCNDEFReaderSession, didInvalidateWithError error: Error) {
        print("Error reading NFC: \(error.localizedDescription)")
    }
    
    func readerSession(_ session: NFCNDEFReaderSession, didDetectNDEFs messages: [NFCNDEFMessage]) {
        for message in messages {
            for record in message.records {
                if let string = String(data: record.payload, encoding: .ascii) {
                    bleDeviceName = string
                    print("------BLE DERVICE NAME FROM TAG -----")
                    print(bleDeviceName)
                    // write to database
                    preferences.set(bleDeviceName, forKey: "pairedDevice")
                }
            }
        }
        BLEDevice.shared.initCBManager()
        // Time to wait until NFC dialog disappear
        sleep(UInt32(3.0))
        notificationWebView.evaluateJavaScript("activityIndicator()")
        // From here the controller waits for the connection status from Singleton via Notification center
    }
    
    // Notification function called when device is connected
    @objc func connectionEstablished(_ notificaiton:Notification) {
        notificationWebView.evaluateJavaScript("connSuccess()")
    }

    // Notification function called when scan interval timed out
    @objc func timeOutNotification(_ notificaiton:Notification) {
        if self.isViewLoaded && (self.view.window != nil) {
            timeOutInterval()
            
        }
    }
    
    func timeOutInterval() {
        //blur background
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.regular)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.view.addSubview(blurEffectView)
        
        popUpView.center = self.view.center
        popUpView.layer.cornerRadius = 10
        self.view.addSubview(popUpView)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
