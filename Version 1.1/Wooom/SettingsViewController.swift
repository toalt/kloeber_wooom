//
//  SettingsViewController.swift
//  Wooom
//
//  Created by Tobias Alt on 10.08.18.
//  Copyright © 2018 Kloeber. All rights reserved.
//

import UIKit


class SettingsViewController: UIViewController {
    
    // IBOutlets
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var connectinState: UILabel!
    @IBOutlet weak var stateView: UIView!
    @IBOutlet weak var popUpView2: UIView!
    @IBOutlet weak var discBtn: UIButton!
    @IBOutlet weak var legalView: UIView!
    @IBOutlet weak var aboutView: UIView!
    
    // Create a global instance of NSUserDefaults class
    let preferences = UserDefaults.standard
    
    // Notication Center for update device state
    let nc = NotificationCenter.default
    
    //corparate color
    let primaryColor = UIColor(rgb: 0xFFBB02)
    
    var language: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //get current language
        language = Locale.current.languageCode
        
        // Add onclick event to each view
        let tapGestureRecognizerState = UITapGestureRecognizer(target: self, action: #selector(onStateTapped(gestureRecognizer:)))
        stateView.addGestureRecognizer(tapGestureRecognizerState)
        
        let tapGestureRecognizerLegal = UITapGestureRecognizer(target: self, action: #selector(onLegalTapped(gestureRecognizer:)))
        legalView.addGestureRecognizer(tapGestureRecognizerLegal)
        
        let tapGestureRecognizerAbout = UITapGestureRecognizer(target: self, action: #selector(onAboutTapped(gestureRecognizer:)))
        aboutView.addGestureRecognizer(tapGestureRecognizerAbout)
        
        nc.addObserver(self, selector: #selector(lostConnection(_:)), name: Notification.Name("BLEDisconnected"), object: nil)
        
        discBtn.backgroundColor = primaryColor
        discBtn.layer.cornerRadius = 10
        
        // write ID to settings label 
        setId()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @objc func onStateTapped(gestureRecognizer: UIGestureRecognizer) {
        self.performSegue(withIdentifier: "showConState", sender: nil)
    }
    
    @objc func onLegalTapped(gestureRecognizer: UIGestureRecognizer) {
        switch (language) {
        case "de":
            guard let url = URL(string: "https://www.kloeber.com/de-de/datenschutzerklaerung/") else { return }
            UIApplication.shared.open(url)
            break
        case "en":
            guard let url = URL(string: "https://www.kloeber.com/kl-en/data-privacy-policy/") else { return }
            UIApplication.shared.open(url)
            break
        case "fr":
            guard let url = URL(string: "https://www.kloeber.com/fr-fr/declaration-de-protection-des-donnees/") else { return }
            UIApplication.shared.open(url)
            break
        case "nl":
            guard let url = URL(string: "https://www.kloeber.com/nl-nl/verklaring-mbt-de-bescherming-van-persoonsgegevens/") else { return }
            UIApplication.shared.open(url)
            break
        default:
            guard let url = URL(string: "https://www.kloeber.com/kl-en/data-privacy-policy/") else { return }
            UIApplication.shared.open(url)
            break
        }
    }
    
    @objc func onAboutTapped(gestureRecognizer: UIGestureRecognizer) {
        switch (language) {
        case "de":
            guard let url = URL(string: "https://www.kloeber.com/de-de/kloeber-welt/wir-ueber-uns/") else { return }
            UIApplication.shared.open(url)
            break
        case "en":
            guard let url = URL(string: "https://www.kloeber.com/kl-en/kloeber-world/welcome-to-kloeber/") else { return }
            UIApplication.shared.open(url)
            break
        case "fr":
            guard let url = URL(string: "https://www.kloeber.com/fr-fr/lunivers-kloeber/a-propos-de-nous/") else { return }
            UIApplication.shared.open(url)
            break
        case "nl":
            guard let url = URL(string: "https://www.kloeber.com/nl-nl/kloeber-wereld/over-kloeber/") else { return }
            UIApplication.shared.open(url)
            break
        default:
            guard let url = URL(string: "https://www.kloeber.com/kl-en/kloeber-world/welcome-to-kloeber/") else { return }
            UIApplication.shared.open(url)
            break
        }
    }
    
    func setId() {
        
        if(BLEDevice.shared.isConnected == true) {
            // fetch the preferences
            let storedDeviceName = preferences.object(forKey: "pairedDevice") as! String
            switch (language) {
            case "de":
                connectinState.text = "Stuhl verbunden"
                break
            case "en":
                connectinState.text = "Chair connected"
                break
            case "fr":
                connectinState.text = "Fauteuil connecté"
                break
            case "nl":
                connectinState.text = "Stoel verbonden"
                break
            default:
                connectinState.text = "Chair connected"
                break
            }            
            idLabel.text = storedDeviceName
        } else {
            switch (language) {
            case "de":
                connectinState.text = "Stuhl nicht verbunden"
                break
            case "en":
                connectinState.text = "Chair disconnected"
                break
            case "fr":
                connectinState.text = "Fauteuil non connecté"
                break
            case "nl":
                connectinState.text = "Stoel niet verbonden"
                break
            default:
                connectinState.text = "Chair disconnected"
                break
            }
        }
    }
    
    // Notification function called when device gets disconnected
    @objc func lostConnection(_ notificaiton:Notification) {
        disconnected()
    }
    
    func disconnected() {
        //blur background
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.regular)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.view.addSubview(blurEffectView)
        
        popUpView2.center = self.view.center
        popUpView2.layer.cornerRadius = 10
        self.view.addSubview(popUpView2)
        
        tabBarController?.tabBar.isHidden = true
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}


